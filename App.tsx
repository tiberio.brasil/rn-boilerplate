import { BreeSerif_400Regular, useFonts } from '@expo-google-fonts/bree-serif';
import {
  NotoSans_400Regular,
  NotoSans_700Bold,
} from '@expo-google-fonts/noto-sans';
import AppLoading from 'expo-app-loading';
import React from 'react';
import { StatusBar } from 'react-native';
import 'react-native-gesture-handler';
import { ThemeProvider } from 'styled-components';
import theme from './src/global/styles/theme';
import { AppProvider } from './src/hooks';

// Telas para desenvolvimento antes de configurar as rotas
import Navigation from './src/routes';

export default function App() {
  const [fontsLoaded] = useFonts({
    BreeSerif_400Regular,
    NotoSans_400Regular,
    NotoSans_700Bold,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <ThemeProvider theme={theme}>
      <AppProvider>
        <StatusBar hidden />

        <Navigation />
      </AppProvider>
    </ThemeProvider>
  );
}
