export interface IBranch {
  id: string;
  street: string;
  number: number;
  complement: string;
  district: string;
  cep: string;
  city: string;
  uf: string;
}

export interface ICategory {
  id: string;
  number: number;
  name: string;
  thumbnail_url: string;
  is_active: Boolean;
  item_class: string;
  created_at: string;
  updated_at: string;
}

export interface IProduct {
  cart_quantity: number;
  id: string;
  name: string;
  thumbnail_url?: string;
  code?: string;
  group_code?: string;
  bar_code?: string;
  tree_type?: string;
  sales_unit?: string;
  sales_unit_length?: string;
  sales_unit_width?: string;
  sales_unit_height?: string;
  sales_unit_volume?: string;
  sales_unit_weight?: string;
  description?: string;
  price: number;
  points_price?: string;
  points_gen?: string;
  on_sale_price?: string;
  quantity: number;
  featured: boolean;
  is_active: boolean;
  is_on_sale: boolean;
  is_fragile: boolean;
  created_at: string;
  updated_at: string;
  categories: string[];
  is_pick_up: boolean;
  is_delivery: boolean;
  product_prices: IProductPrices[];
  product_warehouses: IProductWarehouses[];
  promotions: IPromotions[];
}

interface IProductPrices {
  id: string;
  price_list: number;
  price: number;
  on_sale_price?: number;
  currency: string;
  additional_price1: number;
  additional_price2: number;
  additional_currency1: number | null;
  additional_currency2: number | null;
  base_price_list: number;
  factor: number;
  created_at: string;
  updated_at: string;
  product_id: string;
}

interface IProductWarehouses {
  id: string;
  minimal_stock: number;
  maximal_stock: number;
  minimal_order: number;
  standard_avg_price: number;
  warehouse_code: string;
  in_stock: number;
  committed: number;
  ordered: number;
  was_counted: string;
  counted: number;
  item_code: string;
  ind_escala: string;
  created_at: string;
  updated_at: string;
  product_id: string;
}

interface IPromotions {
  id: string;
  title: string;
  description: string;
  is_active: Boolean;
  discount_type: string;
  value: number;
  initial_date: string;
  expiry_date: string;
  initial_time: string;
  final_time: string;
  banner_app: string | null;
  banner_desktop: string | null;
  banner_mobile: string | null;
  created_at: string;
  updated_at: string;
}

export interface IProductCart {
  product: {
    id: string;
  };
  quantity: number;
}
