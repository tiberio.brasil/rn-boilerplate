export declare global {
  namespace ReactNavigation {
    interface RootParamList {
      Loja: undefined;
      SplashScreen: undefined;
      Local: undefined;
      Fidelidade: undefined;
      Categorias: undefined;
      Busca: undefined;
      Identificacao: undefined;
      CategoriaLista: { id: number };
      Detalhes: { id: string };
      Carrinho: undefined;
    }
  }
}
