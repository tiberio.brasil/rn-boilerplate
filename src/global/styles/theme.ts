export default {
  colors: {
    primary: '#FA2458',
    secondary: '',

    success: '#59C7A3',
    attention: '#FC4A4E',

    title: '#FFFFFF',
    background: '#FFFFFF',
    backgroundGray: '#F5F5F5',
    backgroundGraySelected: '#E7E7E7',
    backgroundGrayLight: '#F8F8F8',

    border: '#D3D3D3',

    text: '#231F20',
    textLight: '#BDBDBD',
    textGray: '#4F4F4F',

    white: '#FFFFFF',
    black: '#000000',

    header: '#FFFFFF',

    bottomTabBar: {
      active: {
        color: '#FFFFFF',
        background: '#FA2458',
      },
      inactive: {
        color: '#FA2458',
        background: '#FED278',
      },
      background: '#FED278',
    },

    input: {
      label: '#333333',
      placeholder: '#BDBCBC',
      text: '#656263',
      border: '#BDBCBC',
      borderFocused: '#59C7A3',
      background: '#FFFFFF',
    },

    submit: {
      primary: {
        color: '#FFFFFF',
        background: '#FA2458',
      },
      secondary: {
        color: '#FA2458',
        background: '#FFFFFF',
      },
    },

    modal: {
      background: 'rgba(0, 0, 0, 0.6);',
      button: {
        background: '#F8F8F8',
        negativeContent: '#FC4A4E',
        positiveContent: '#59C7A3',
      },
    },
  },

  fonts: {
    regular: 'BreeSerif_400Regular',

    alternative: {
      regular: 'NotoSans_400Regular',
      bold: 'NotoSans_700Bold',
    },
  },
};
