export const ASYNC_STORAGE_APP_NAME = '@SucreClienteMobile';

export enum EnumTipoPedido {
  comerAqui = 'comer-aqui',
  vouLevar = 'vou-levar',
}

export enum EnumFidelidade {
  sim = 'sim',
  nao = 'nao',
}

export const asyncStorageNames = [
  `${ASYNC_STORAGE_APP_NAME}:tipoPedido`,
  `${ASYNC_STORAGE_APP_NAME}:fidelidade`,
  `${ASYNC_STORAGE_APP_NAME}:cpf`,
];

export const CPF_MASK = [
  /\d/,
  /\d/,
  /\d/,
  '.',
  /\d/,
  /\d/,
  /\d/,
  '.',
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
];
