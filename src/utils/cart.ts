import { IProductCart } from '../@types/interfaces';
import { getAsyncStorage } from './asyncStorage';

export const addItemToCart = async (id: string, quantity: number = 1) => {
  const cart: IProductCart[] = await getAsyncStorage('cart');
  let newCart: IProductCart[];

  if (!cart) {
    console.log('cart', cart);
  } else {
    const checkIfProductAlreadyExistsOnCart = cart.filter(
      (item) => item.product.id
    );

    console.log(
      'checkIfProductAlreadyExistsOnCart',
      checkIfProductAlreadyExistsOnCart
    );

    console.log('cart', cart);
    console.log({ id, quantity });
  }
};
