import AsyncStorage from '@react-native-async-storage/async-storage';
import { asyncStorageNames, ASYNC_STORAGE_APP_NAME } from './constants';

export const setAsyncStorage = async (key: string, value: string) => {
  try {
    await AsyncStorage.setItem(`${ASYNC_STORAGE_APP_NAME}:${key}`, `${value}`);
  } catch (error) {
    console.warn(error);
  }
};

export const getAsyncStorage = async (key: string) => {
  try {
    const getAsyncStorageData = await AsyncStorage.getItem(
      `${ASYNC_STORAGE_APP_NAME}:${key}`
    );
    if (!getAsyncStorageData) return null;

    let getAsyncStorageParsed;

    if (typeof getAsyncStorageData === 'string') {
      getAsyncStorageParsed = getAsyncStorageData;
    } else {
      getAsyncStorageParsed = JSON.parse(getAsyncStorageData);
    }

    return getAsyncStorageParsed;
  } catch (error) {
    console.warn(error);
  }
};

export const multiRemoveAsyncStorage = async () => {
  await AsyncStorage.multiRemove(asyncStorageNames);
};
