export function formatPrice(price: number) {
  return price
    .toFixed(2)
    .toString()
    .replace('.', ',')
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}
