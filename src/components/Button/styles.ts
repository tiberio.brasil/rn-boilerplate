import { TouchableOpacity } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import styled from "styled-components/native";

interface ContainerProps {
  type: "primary" | "secondary";
}

export const Container = styled(TouchableOpacity)<ContainerProps>`
  width: 100%;
  justify-content: center;
  align-items: center;

  height: 40px;
  padding: 0 16px;
  background-color: ${({ theme, type }) =>
    type === "primary"
      ? theme.colors.submit.primary.background
      : theme.colors.submit.secondary.background};

  border-radius: 20px;
  margin-top: 12px;
  margin-bottom: 12px;

  shadow-color: #000;
  shadow-radius: 20px;
  elevation: 10;
`;

interface TitleProps {
  type: "primary" | "secondary";
}

export const Title = styled.Text<TitleProps>`
  width: 100%;
  font-family: ${({ theme }) => theme.fonts.regular};
  font-size: ${RFValue(16)}px;
  color: ${({ theme, type }) =>
    type === "primary"
      ? theme.colors.submit.primary.color
      : theme.colors.submit.secondary.color};

  text-align: center;
`;
