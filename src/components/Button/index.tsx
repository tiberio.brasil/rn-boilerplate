import React from "react";
import { ActivityIndicator, TouchableOpacityProps } from "react-native";
import { useTheme } from "styled-components";
import * as S from "./styles";
import { Ionicons } from "@expo/vector-icons";

interface Props extends TouchableOpacityProps {
  title?: string;
  icon?: React.ComponentProps<typeof Ionicons>["name"];
  type?: "primary" | "secondary";
  loading?: boolean;
  onPress: () => void;
  enabled?: boolean;
}

export function Button({
  title,
  icon,
  type,
  loading = false,
  onPress,
  enabled = true,
}: Props) {
  const theme = useTheme();
  const buttonType = type === "secondary" ? "secondary" : "primary";

  return (
    <S.Container
      onPress={onPress}
      style={{ opacity: enabled === false || loading === true ? 0.5 : 1 }}
      type={buttonType}
    >
      {icon && (
        <Ionicons
          name={icon}
          size={24}
          color={
            buttonType === "primary"
              ? theme.colors.submit.primary.color
              : theme.colors.submit.secondary.color
          }
        />
      )}

      {loading ? (
        <ActivityIndicator color={theme.colors.primary} />
      ) : title && !icon ? (
        <S.Title type={buttonType}>{title}</S.Title>
      ) : (
        <></>
      )}
    </S.Container>
  );
}
