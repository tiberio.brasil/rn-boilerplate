import React from "react";
import { Control, Controller } from "react-hook-form";
import { IComponentInputPasswordProps, InputPassword } from "../InputPassword";
import * as S from "./styles";

interface Props extends IComponentInputPasswordProps {
  control: Control;
  name: string;
  error: string;
}

export function InputPasswordForm({ control, name, error, ...rest }: Props) {
  return (
    <S.Container>
      <Controller
        control={control}
        render={({ field: { onChange, value } }) => (
          <InputPassword onChangeText={onChange} value={value} {...rest} />
        )}
        name={name}
      />
      {error && <S.Error>{error}</S.Error>}
    </S.Container>
  );
}
