import React from "react";

import * as S from "./styles";

interface Props {
  iconLeft?: "menu" | "back";
  title: string;
  iconRight?: "search";
}

export const Header = ({ iconLeft, title, iconRight }: Props) => {
  return (
    <S.Container>
      <S.HeaderWrapper>
        <S.ViewLeft></S.ViewLeft>

        <S.ViewCenter>
          <S.ViewCenterText>{title}</S.ViewCenterText>
        </S.ViewCenter>

        <S.ViewRight></S.ViewRight>
      </S.HeaderWrapper>
    </S.Container>
  );
};
