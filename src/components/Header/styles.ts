import { RFValue } from "react-native-responsive-fontsize";
import styled from "styled-components/native";
import { getStatusBarHeight } from "react-native-iphone-x-helper";

export const Container = styled.View`
  width: 100%;
  padding-top: ${getStatusBarHeight()}px;

  justify-content: center;
  margin-bottom: ${RFValue(6)}px;
  background-color: ${({ theme }) => theme.colors.primary};
`;

export const HeaderWrapper = styled.View`
  height: ${RFValue(48)}px;
  justify-content: center;
  align-items: center;
`;

export const ViewLeft = styled.View``;

export const ViewCenter = styled.View``;

export const ViewRight = styled.View``;

export const ViewCenterText = styled.Text`
  font-family: ${({ theme }) => theme.fonts.regular};
  font-size: ${RFValue(16)}px;
  line-height: 22px;
  color: ${({ theme }) => theme.colors.header};
`;
