import { Ionicons } from '@expo/vector-icons';
import React from 'react';
import { Image, ImageSourcePropType } from 'react-native';
import ReacNativeModal from 'react-native-modal';
import * as S from './styles';

export interface ModalProps {
  content?: string;
  enabled?: boolean;
  icon?: React.ComponentProps<typeof Ionicons>['name'];
  image?: ImageSourcePropType;
  loading?: boolean;
  cancelButtonContent?: string;
  onPressCancelButton?: () => void;
  onPressConfirmButton?: () => void;
  confirmButtonContent?: string;
  isVisible?: boolean;
  toggleModal?: () => void;
  type: 'dual' | 'mono';
}

function Modal({
  content,
  enabled,
  image,
  icon,
  cancelButtonContent,
  confirmButtonContent,
  loading,
  onPressCancelButton,
  onPressConfirmButton,
  isVisible,
  toggleModal,
  type,
}: ModalProps) {
  return (
    <ReacNativeModal
      isVisible={isVisible}
      onBackdropPress={toggleModal}
      backdropTransitionOutTiming={0}
    >
      <S.ContentContainer>
        <S.IconContainer>
          {image ? (
            <Image source={image} width={50} height={50} resizeMode="contain" />
          ) : (
            <Ionicons name={icon || 'md-alert-outline'} size={35} color="red" />
          )}
        </S.IconContainer>

        <S.Content>{content || 'Ops! Ocorreu um erro'}</S.Content>

        <S.ButtonContainer>
          <S.RightButton
            onPress={onPressConfirmButton}
            isDual={type === 'dual'}
          >
            <S.PositiveButtonContent>
              {confirmButtonContent || 'OK'}
            </S.PositiveButtonContent>
          </S.RightButton>

          {type === 'dual' ? (
            <S.LeftButton onPress={onPressCancelButton}>
              <S.NegativeButtonContent>
                {cancelButtonContent || 'CANCELAR'}
              </S.NegativeButtonContent>
            </S.LeftButton>
          ) : null}
        </S.ButtonContainer>
      </S.ContentContainer>
    </ReacNativeModal>
  );
}

export default Modal;
