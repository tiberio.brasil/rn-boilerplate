import { PressableProps } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';
import { DefaultText } from '../../global/styles/styles';

export const Container = styled.View`
  width: 100%;
  height: 100%;
  position: absolute;

  background-color: ${({ theme }) => theme.colors.modal.background};

  align-self: center;
  align-items: center;
  justify-content: center;
`;

export const ContentContainer = styled.View`
  min-width: ${RFValue(280)}px;
  min-height: ${RFValue(127)}px;

  background-color: ${({ theme }) => theme.colors.white};

  border-radius: ${RFValue(8)}px;

  align-items: center;
  justify-content: center;

  padding: ${RFValue(18)}px;
`;

export const IconContainer = styled.View`
  margin-bottom: ${RFValue(8)}px;
`;

export const Content = styled(DefaultText)``;

export const NegativeButtonContent = styled(Content)`
  color: ${({ theme }) => theme.colors.modal.button.negativeContent};
`;
export const PositiveButtonContent = styled(Content)`
  color: ${({ theme }) => theme.colors.modal.button.positiveContent};
`;

export const ButtonContainer = styled.View`
  margin-top: ${RFValue(20)}px;

  flex-direction: row;
`;

export const LeftButton = styled.Pressable`
  min-width: ${RFValue(108)}px;
  min-height: ${RFValue(36)}px;

  background-color: ${({ theme }) => theme.colors.modal.button.background};

  border-radius: ${RFValue(20)}px;

  align-items: center;
  justify-content: center;

  shadow-offset: 0px 3px;
  shadow-radius: 10px;
  elevation: ${RFValue(3)};
`;

interface RightButtonProps extends PressableProps {
  isDual: boolean;
}

export const RightButton = styled(LeftButton)<RightButtonProps>`
  margin-right: ${({ isDual }) => (isDual ? RFValue(16) : 0)}px;
`;
