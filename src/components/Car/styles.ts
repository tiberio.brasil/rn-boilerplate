import styled from "styled-components/native";

export const Container = styled.View`
  width: 100%;
  height: 586px;
  position: absolute;
  bottom: 0;
  background-color: ${({ theme }) => theme.colors.backgroundGrayLight};
  border-top-width: 1px;
  border-top-color: ${({ theme }) => theme.colors.backgroundGraySelected};
  border-top-left-radius: 16px;
  border-top-right-radius: 16px;
`;

export const TitleCart = styled.Text`
  font-size: 24px;
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.regular};
  text-align: center;
`;

export const ProductArea = styled.View`
  margin-top: 22px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 20px;
`;

export const ProductImage = styled.Image`
  width: 50px;
  height: 50px;
`;

export const ProductName = styled.Text`
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.text};
  font-size: 18px;
  margin-left: 23px;
`;

export const ProductLeftArea = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ProductAreaRight = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Quantity = styled.Text`
  font-size: 24px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.text};
`;

export const Price = styled.Text`
  font-size: 20px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.text};
  margin: 20px;
`;

export const ButtonDeleteProduct = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  justify-content: center;
  align-items: center;
  width: 100px;
  margin: 20px;
  border-radius: 50px;
  padding: 10px;
`;

export const ActionProduct = styled.TouchableOpacity`
  padding: 10px;
  margin: 25px;
  border-radius: 99px;
  border-width: 1px;
  border-color: ${({ theme }) => theme.colors.backgroundGraySelected};
`;

export const Divider = styled.View`
  width: 95%;
  height: 2px;
  background-color: ${({ theme }) => theme.colors.input.border};
  align-self: center;
`;

export const SubtotalArea = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 30px;
`;

export const SubtotalPrice = styled.Text`
  font-size: 20px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.text};
  margin-right: 23px;
`;

export const TextInputObservations = styled.TextInput`
  background-color: ${({ theme }) => theme.colors.background};
  width: 90%;
  align-self: center;
  border-radius: 50px;
  font-size: 20px;
  padding: 10px;
  margin-top: 22px;
  font-family: ${({ theme }) => theme.fonts.regular};
`;

export const ButtonCancel = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.textLight};
  align-items: center;
  justify-content: center;
  border-radius: 50px;
  height: 72px;
  width: 40%;
`;

export const ButtonConfirm = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  align-items: center;
  justify-content: center;
  border-radius: 50px;
  height: 72px;
  width: 40%;
`;

export const TitleConfirm = styled.Text`
  font-size: 24px;
  color: ${({ theme }) => theme.colors.title};
  font-family: ${({ theme }) => theme.fonts.regular};
`;

export const ButtonsActions = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  margin-top: 30px;
`;