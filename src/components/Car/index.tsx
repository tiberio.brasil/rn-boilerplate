import React from "react";

import * as S from "./styles";
import { FontAwesome5 } from "@expo/vector-icons";
import { useTheme } from "styled-components";

import Produto from "./../../assets/image/brigadeiro.png";

// interface Props {
//   iconLeft?: "menu" | "back";
//   title: string;
//   iconRight?: "search";
// }

export const Car = () => {
  const theme = useTheme();

  return (
    <S.Container>
      <S.TitleCart>Seu Carrinho</S.TitleCart>

      <S.ProductArea>
        <S.ProductLeftArea>
          <S.ProductImage source={Produto}/>
          <S.ProductName>BRIGADEIRO BELGA</S.ProductName>
        </S.ProductLeftArea>

        <S.ProductAreaRight>
          <S.ActionProduct>
            <FontAwesome5
              name={"minus"}
              size={24}
              color={theme.colors.attention}
            />
          </S.ActionProduct>
          <S.Quantity>3</S.Quantity>
          <S.ActionProduct>
            <FontAwesome5
              name={"plus"}
              size={24}
              color={theme.colors.success}
            />
          </S.ActionProduct>

          <S.Price>R$ 0,00</S.Price>
          <S.ButtonDeleteProduct>
            <FontAwesome5
              name={"trash"}
              color={theme.colors.background}
              size={24}
            />
          </S.ButtonDeleteProduct>
        </S.ProductAreaRight>
      </S.ProductArea>

      <S.Divider />

      <S.SubtotalArea>
        <S.ProductName>SUBTOTAL</S.ProductName>
        <S.SubtotalPrice>R$ 0,00</S.SubtotalPrice>
      </S.SubtotalArea>
      <S.TitleCart>Observações</S.TitleCart>

      <S.TextInputObservations 
        placeholder="Digite aqui as observações"
        placeholderTextColor={theme.colors.input.text}
      />

      <S.ButtonsActions>
        <S.ButtonCancel>
          <S.TitleCart>Cancelar</S.TitleCart>
        </S.ButtonCancel>

        <S.ButtonConfirm>
          <S.TitleConfirm>Confirmar pedido</S.TitleConfirm>
        </S.ButtonConfirm>
      </S.ButtonsActions>
    </S.Container>
  );
};
