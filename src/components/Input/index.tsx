import React, { useState } from "react";
import { TextInputProps } from "react-native";
import { Mask } from "react-native-mask-input";

import * as S from "./styles";

export interface IComponentInputProps extends TextInputProps {
  label?: string;
  disabled?: boolean;
  mask?: Mask;
  error?: {};
}

export function Input({
  value,
  label,
  disabled,
  mask,
  error,
  ...rest
}: IComponentInputProps) {
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);

  function handleInputFocus() {
    setIsFocused(true);
  }

  function handleInputBlur() {
    setIsFocused(false);
    setIsFilled(!!value);
  }

  return (
    <S.Container isFocused={isFocused} isErrored={!!error}>
      {label && <S.InputLabel>{label}</S.InputLabel>}

      <S.InputText
        onFocus={handleInputFocus}
        onBlur={handleInputBlur}
        disabled={!!disabled}
        editable={!disabled}
        selectTextOnFocus={!disabled}
        value={value || ""}
        mask={mask}
        {...rest}
      />
    </S.Container>
  );
}
