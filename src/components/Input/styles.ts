import MaskInput from 'react-native-mask-input';
import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';

interface DisabledProps {
  disabled: boolean;
}

interface ContainerProps {
  isFocused: boolean;
  isErrored: boolean;
}

export const Container = styled.View<ContainerProps>`
  width: 100%;
  justify-content: center;
  align-items: center;

  height: 36px;
  padding: 0 16px;
  background: ${({ theme }) => theme.colors.input.background};

  border-radius: 48px;

  border-width: 0px;
  border-color: ${({ theme, isFocused, isErrored }) =>
    isFocused
      ? theme.colors.input.borderFocused
      : isErrored
      ? theme.colors.attention
      : theme.colors.input.border};
`;

export const InputLabel = styled.Text`
  position: absolute;
  z-index: 1;
  top: -10px;
  left: 16px;

  padding: 0 4px;

  font-family: ${({ theme }) => theme.fonts.regular};
  font-size: ${RFValue(12)}px;
  color: ${({ theme }) => theme.colors.input.text};
  background-color: ${({ theme }) => theme.colors.white};
`;

export const InputText = styled(MaskInput)<DisabledProps>`
  flex: 1;
  width: 100%;
  font-family: ${({ theme }) => theme.fonts.alternative.regular};
  font-size: ${RFValue(16)}px;
  color: ${({ theme, disabled }) =>
    disabled ? theme.colors.white : theme.colors.text};
  background-color: ${({ theme }) => theme.colors.input.background};
  border-radius: 48px;
`;
