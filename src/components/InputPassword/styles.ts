import styled, { css } from "styled-components/native";
import { RFValue } from "react-native-responsive-fontsize";
import { TextInput } from "react-native";

interface DisabledProps {
  disabled: boolean;
}

interface ContainerProps {
  isFocused: boolean;
  isErrored: boolean;
}

export const Container = styled.View<ContainerProps>`
  width: 100%;
  height: 48px;
  padding: 0 16px;
  background: ${({ theme }) => theme.colors.white};

  border-radius: 8px;
  margin-top: 12px;
  margin-bottom: 12px;

  border-width: 1px;
  border-color: ${({ theme, isFocused }) =>
    isFocused ? theme.colors.input.borderFocused : theme.colors.input.border};

  flex-direction: row;
  align-items: center;

  ${(props) =>
    props.isErrored &&
    css`
      border-color: #c53030;
    `}
  ${(props) =>
    props.isFocused &&
    css`
      border-color: ${({ theme }) => theme.colors.input.borderFocused};
    `}
`;

export const Wrapper = styled.View`
  flex-direction: row;
`;

export const IconContainer = styled.View`
  justify-content: center;
  align-items: center;
  margin-right: 2px;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const InputLabel = styled.Text`
  position: absolute;
  top: -10px;
  left: 16px;

  padding: 0 4px;

  font-family: ${({ theme }) => theme.fonts.regular};
  font-size: ${RFValue(12)}px;
  color: ${({ theme }) => theme.colors.input.text};
  background-color: ${({ theme }) => theme.colors.white};
`;

export const InputText = styled(TextInput)<DisabledProps>`
  flex: 1;
  font-family: ${({ theme }) => theme.fonts.regular};
  font-size: ${RFValue(16)}px;
  color: ${({ theme, disabled }) =>
    disabled ? theme.colors.background : theme.colors.text};
  background-color: ${({ theme }) => theme.colors.input.background};
`;
