import React, { useState } from "react";
import { TextInputProps, TouchableOpacity } from "react-native";
import { useTheme } from "styled-components";
import * as S from "./styles";
import { Feather } from "@expo/vector-icons";

export interface IComponentInputPasswordProps extends TextInputProps {
  value?: string;
  label?: string;
  disabled?: boolean;
  type?: string;
}

export function InputPassword({
  value,
  label,
  disabled,
  type,
  ...rest
}: IComponentInputPasswordProps) {
  const [isPasswordVisible, setIsPasswordVisible] = useState(true);
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);

  const theme = useTheme();

  function handleInputFocus() {
    setIsFocused(true);
  }

  function handleInputBlur() {
    setIsFocused(false);
    setIsFilled(!!value);
  }

  function handlePasswordVisibilityChange() {
    setIsPasswordVisible((prevState) => !prevState);
  }

  return (
    <S.Container isFocused={isFocused} isErrored={false}>
      {label && <S.InputLabel>{label}</S.InputLabel>}

      <S.InputText
        onFocus={handleInputFocus}
        onBlur={handleInputBlur}
        disabled={!!disabled}
        editable={!disabled}
        selectTextOnFocus={!disabled}
        {...rest}
      />
      <S.Wrapper>
        <S.InputText
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
          secureTextEntry={isPasswordVisible}
          disabled={!!disabled}
          editable={!disabled}
          selectTextOnFocus={!disabled}
          autoCorrect={false}
          {...rest}
        />

        <TouchableOpacity onPress={handlePasswordVisibilityChange}>
          <S.IconContainer>
            <Feather
              name={isPasswordVisible ? "eye" : "eye-off"}
              size={24}
              color={
                isFocused
                  ? theme.colors.input.borderFocused
                  : theme.colors.input.text
              }
            />
          </S.IconContainer>
        </TouchableOpacity>
      </S.Wrapper>
    </S.Container>
  );
}
