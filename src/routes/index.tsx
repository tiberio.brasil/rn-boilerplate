import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import { useAuth } from '../hooks/auth';
import TotemRoutes from './app.routes';

const Routes: React.FC = () => {
  const { loading } = useAuth();

  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator color='#999' size='large' />
      </View>
    );
  }

  return (
    <NavigationContainer>
      <TotemRoutes />
    </NavigationContainer>
  );
};

export default Routes;
