import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { Busca } from '../screens/Busca';
import { Carrinho } from '../screens/Carrinho';
import { CategoriaLista } from '../screens/CategoriaLista';
import { Categorias } from '../screens/Categorias';
import { Detalhes } from '../screens/Detalhes';
import { Fidelidade } from '../screens/Fidelidade';
import { Identificacao } from '../screens/Identificacao';
import { Local } from '../screens/Local';
import { Loja } from '../screens/Loja';
import { SplashScreen } from '../screens/SplashScreen';

const AppRoutes = createStackNavigator();

const TotemRoutes = () => {
  return (
    <AppRoutes.Navigator
      initialRouteName='SplashScreen'
      screenOptions={{
        headerShown: false,
      }}
    >
      <AppRoutes.Screen name='Loja' component={Loja} />
      <AppRoutes.Screen name='SplashScreen' component={SplashScreen} />
      <AppRoutes.Screen name='Local' component={Local} />
      <AppRoutes.Screen name='Fidelidade' component={Fidelidade} />
      <AppRoutes.Screen name='Categorias' component={Categorias} />
      <AppRoutes.Screen name='Busca' component={Busca} />
      <AppRoutes.Screen name='Identificacao' component={Identificacao} />
      <AppRoutes.Screen name='CategoriaLista' component={CategoriaLista} />
      <AppRoutes.Screen name='Detalhes' component={Detalhes} />
      <AppRoutes.Screen name='Carrinho' component={Carrinho} />
    </AppRoutes.Navigator>
  );
};

export default TotemRoutes;
