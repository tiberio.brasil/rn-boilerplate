import React, { ReactNode } from 'react';

import { AuthProvider } from './auth';
import { ModalProvider } from './modal';

interface AppProviderProps {
  children: ReactNode;
}

function AppProvider({ children }: AppProviderProps) {
  return (
    <ModalProvider>
      <AuthProvider>{children}</AuthProvider>
    </ModalProvider>
  );
}

export { AppProvider };
