import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import api from "../services/api";
import { ASYNC_STORAGE_APP_NAME } from "../utils/constants";

interface User {
  id: string;
  name: string;
  email: string;
  permissions: {
    edit_permissions: boolean;
    read_permissions: boolean;
    create_employee: boolean;
    edit_employee: boolean;
    delete_employee: boolean;
    read_employee: boolean;
    create_client: boolean;
    edit_client: boolean;
    delete_client: boolean;
    read_client: boolean;
    create_category: boolean;
    edit_category: boolean;
    delete_category: boolean;
    create_role: boolean;
    read_role: boolean;
    edit_role: boolean;
    delete_role: boolean;
    create_product: boolean;
    edit_product: boolean;
    delete_product: boolean;
    read_product: boolean;
    create_branch: boolean;
    edit_branch: boolean;
    delete_branch: boolean;
    read_branch: boolean;
    create_delivery_radius: boolean;
    edit_delivery_radius: boolean;
    delete_delivery_radius: boolean;
    read_delivery_radius: boolean;
    create_notification: boolean;
    edit_notification: boolean;
    read_notification: boolean;
    create_coupon: boolean;
    edit_coupon: boolean;
    delete_coupon: boolean;
    read_coupon: boolean;
    create_advertisement: boolean;
    edit_advertisement: boolean;
    delete_advertisement: boolean;
    read_advertisement: boolean;
    suport_access: boolean;
    reports_access: boolean;
    fidelity_access: boolean;
    virtual_wallet_access: boolean;
    chat_access: boolean;
  };
}

interface AuthState {
  user: User;
  token: string;
}

interface SignInCredentials {
  user: string;
  password: string;
}

interface AuthContextData {
  user: User;
  loading: boolean;
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut(): void;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

const AuthProvider: React.FC = ({ children }) => {
  const [data, setData] = useState<AuthState>({} as AuthState);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function loadStoragedData(): Promise<void> {
      const [user, token] = await AsyncStorage.multiGet([
        `${ASYNC_STORAGE_APP_NAME}:user`,
        `${ASYNC_STORAGE_APP_NAME}:token`,
      ]);

      if (user[1] && token[1]) {
        api.defaults.headers.common["Authorization"] = `Bearer ${token[1]}`;

        setData({ user: JSON.parse(user[1]), token: token[1] });
      }

      setLoading(false);
    }

    loadStoragedData();
  }, []);

  const signIn = useCallback(async ({ user, password }) => {
    console.log(">>>>>>>>", { user, password });
    const response = await api.post("/auth/login", {
      user,
      password,
    });

    const userObj = {
      id: response.data.id,
      name: response.data.name,
      email: response.data.email,
      permissions: response.data.role.role_permissions,
    };
    delete userObj.permissions.id;

    const { token } = response.data;

    await AsyncStorage.multiSet([
      [`${ASYNC_STORAGE_APP_NAME}:user`, JSON.stringify(userObj)],
      [`${ASYNC_STORAGE_APP_NAME}:token`, token],
    ]);

    api.defaults.headers.common["Authorization"] = `Bearer ${token[1]}`;

    setData({ user: userObj, token });
  }, []);

  const signOut = useCallback(async () => {
    await AsyncStorage.multiRemove([
      `${ASYNC_STORAGE_APP_NAME}:user`,
      `${ASYNC_STORAGE_APP_NAME}:token`,
    ]);

    setData({} as AuthState);
  }, []);

  return (
    <AuthContext.Provider value={{ user: data.user, loading, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth(): AuthContextData {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error("useAuth must be used within an AuthProvider");
  }

  return context;
}

export { AuthProvider, useAuth };
