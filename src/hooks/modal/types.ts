import { ModalProps } from '../../components/Modal';

interface ModalOptions extends Partial<ModalProps> {
  type?: 'dual' | 'mono';
}

interface ModalContextData {
  showModal: (options?: ModalOptions) => void;
  hideModal: () => void;
}

export { ModalContextData, ModalOptions };
