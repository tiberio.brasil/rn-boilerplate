import React, {
  createContext,
  ReactChild,
  ReactChildren,
  useCallback,
  useContext,
  useState,
} from 'react';
import Modal from '../../components/Modal';
import { ModalContextData, ModalOptions } from './types';

const ModalContext = createContext<ModalContextData>({} as ModalContextData);

interface ModalProviderProps {
  children: ReactChildren | ReactChild;
}

function ModalProvider({ children }: ModalProviderProps) {
  const [isVisible, setIsVisible] = useState(false);
  const [modalOptions, setModalOptions] = useState({} as ModalOptions);

  const showModal = useCallback(
    (options: ModalOptions = {} as ModalOptions) => {
      setModalOptions(options);
      setIsVisible(true);
    },
    [],
  );

  const hideModal = () => {
    setIsVisible(false);
  };

  return (
    <ModalContext.Provider
      value={{
        showModal,
        hideModal,
      }}
    >
      <Modal
        isVisible={isVisible}
        onPressConfirmButton={hideModal}
        toggleModal={() => setIsVisible(!isVisible)}
        type="mono"
        {...modalOptions}
      />
      {children}
    </ModalContext.Provider>
  );
}

function useModal(): ModalContextData {
  const context = useContext(ModalContext);

  if (!context) {
    throw new Error('useModal must be used within a ModalProvider');
  }

  return context;
}

export { ModalProvider, useModal };
