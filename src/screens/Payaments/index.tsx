import React from "react";
import * as S from "./styles";

import { FontAwesome } from "@expo/vector-icons";
import { useTheme } from "styled-components";

import Logo from "../../assets/image/logo.png";
import ArrowLeft from "../../assets/icons/arrow-left-white.png";
import Pix from "../../assets/icons/pix.png";
import Foods from "../../assets/image/iconsFoods.png";

export const Payaments = () => {
  const theme = useTheme();
  const [opPaymanets, setOpPaymanets] = React.useState(false);
  const [process, setProcess] = React.useState(false);
  const [registry, setRegistry] = React.useState(false);
  const [requestSuccess, setRequestSuccess] = React.useState(true);

  return (
    <S.Container>
      <S.Logo source={Logo} />

      <S.Content>
        {opPaymanets &&
          <>
            <S.Header>
              <S.BackButtonArea>
                <S.ArrowLeft source={ArrowLeft} />
              </S.BackButtonArea>
              <S.TitlePedido>Formas de pagamento</S.TitlePedido>
            </S.Header>

            <S.OptionsArea>
              <S.Subtitle>Como deseja realizar o pagamento ?</S.Subtitle>

              <S.ButtonConfirm>
                <FontAwesome
                  name={"credit-card"}
                  size={24}
                  color={theme.colors.background}
                />
                <S.ButtonConfirmText>Crédito</S.ButtonConfirmText>
              </S.ButtonConfirm>

              <S.ButtonConfirm>
                <FontAwesome
                  name={"credit-card"}
                  size={24}
                  color={theme.colors.background}
                />
                <S.ButtonConfirmText>Débito</S.ButtonConfirmText>
              </S.ButtonConfirm>

              <S.ButtonConfirm>
                <S.PixIcon source={Pix} />
                <S.ButtonConfirmText>Pix</S.ButtonConfirmText>
              </S.ButtonConfirm>

            </S.OptionsArea>
          </>
        }

        {process &&
          <>  
            <S.OptionsArea>
              <S.Subtitle>Processando pagamento...</S.Subtitle>
            </S.OptionsArea>
            <S.CancelButton>
              <S.TitleButton>Cancelar</S.TitleButton>
            </S.CancelButton>
          </>
        }

        {registry &&
          <S.OptionsArea>
            <S.IconsFoods source={Foods} />
            <S.Subtitle>Pedido registrado com sucesso!</S.Subtitle>
            <S.NumberRequest>N° #0000</S.NumberRequest>
          </S.OptionsArea>
        }

        {requestSuccess &&
          <>
            <S.OptionsArea>
              <S.IconsFoods source={Foods} />
              <S.TitlePrimary>Não esqueça de levar  seu comprovante</S.TitlePrimary>
              <S.Subtitle>
                Agora é só acompanhar seu pedido{`\n`}
                pelo painel de senhas.
              </S.Subtitle>
              <S.NumberRequest>N° #0000</S.NumberRequest>
            </S.OptionsArea>
            <S.ButtonConfirm>
              <S.ButtonConfirmText>Fazer outro pedido</S.ButtonConfirmText>
            </S.ButtonConfirm>
          </>
        }
      </S.Content>
    </S.Container>
  );
};
