import styled from "styled-components/native";

export const Container = styled.ScrollView`
  flex: 1;
  width: 100%;
`;

export const Content = styled.View`
  flex: 1;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.background};
  padding: 28px 16px 0;
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
`;

export const Logo = styled.Image`
  margin: 30px;
  align-self: center;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
`;

export const BackButtonArea = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  border-radius: 99px;
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px;
`;

export const TitlePedido = styled.Text`
  font-size: 32px;
  font-family: ${({ theme }) => theme.fonts.regular};
  text-align: center;
  width: 90%;
`;

export const ArrowLeft = styled.Image``;

export const OptionsArea = styled.View`
  flex: 1;
  padding: 20px;
  margin-top: 50px;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
`;

export const Categorys = styled.View`
  width: 100%;
  padding: 20px;
  margin-top: 30px;
  flex-direction: row;
  align-items: center;
`;

export const CategoryCard = styled.TouchableOpacity`
  width: 200px;
  align-items: center;
  justify-content: center;
  border-radius: 16px;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.05);
  margin-left: 30px;
`;

export const CategoryImage = styled.Image`
  top: -30px;
`;

export const CategoryName = styled.Text`
  font-size: 22px;
  color: ${({ theme }) => theme.colors.background};
  font-family: ${({ theme }) => theme.fonts.alternative.regular};
  margin-top: 10px;
  top: -10px;
`;

export const ScrollCategory = styled.ScrollView`
  margin-top: 30px;
  max-height: 250px;
`;

export const BottomBar = styled.View`
  width: 100%;
  height: 100%;
  border-radius: 4px;
  height: 8px;
  background-color: ${({ theme }) => theme.colors.bottomTabBar.background};
  top: 8px;
`;

export const CardProduct = styled.TouchableOpacity`
  background-color: rgba(248, 248, 248, 1);
  border-radius: 8px;
  align-items: center;
  justify-content: center;
  margin: 30px;
  width: 224px;
  height: 154px;
  padding: 10px;
`;

export const CardImageProduct = styled.Image`
  width: 144px;
  height: 144px;
  top: -50px;
`;

export const CardNameProduct = styled.Text`
  color: ${({ theme }) => theme.colors.textGray};
  font-family: ${({ theme }) => theme.fonts.regular};
  font-size: 18px;
  top: -50px;
`;

export const CardPriceProduct = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: 24px;
  font-family: ${({ theme }) => theme.fonts.regular};
  top: -40px;
`;