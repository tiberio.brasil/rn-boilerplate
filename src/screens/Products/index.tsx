import React from "react";
import * as S from "./styles";

import Logo from "../../assets/image/logo.png";
import ArrowLeft from "../../assets/icons/arrow-left-white.png";

import Combo from "../../assets/image/combo-image.png";
import Festa from "../../assets/image/festas-image.png";
import Embalados from "../../assets/image/embalados-image.png";
import Salgados from "../../assets/image/salgados-image.png";
import Doces from "../../assets/image/doces-image.png";
import Presentes from "../../assets/image/presentes-image.png";
import Promocoes from "../../assets/image/promocoes-image.png";
import Bebidas from "../../assets/image/bebidas-image.png";

import Brigadeiro from "../../assets/image/brigadeiro.png";

export const Products = () => {
  const categories = [
    {
      title: "Combos",
      image: Combo,
      active: true
    },
    {
      title: "Para festa",
      image: Festa,
      active: false
    },
    {
      title: "Embalados",
      image: Embalados,
      active: false
    },
    {
      title: "Salgados",
      image: Salgados,
      active: false
    },
    {
      title: "Doces",
      image: Doces,
      active: false
    },
    {
      title: "Presentes",
      image: Presentes,
      active: false
    },
    {
      title: "Promoções",
      image: Promocoes,
      active: false
    },
    {
      title: "Bebidas",
      image: Bebidas,
      active: false
    },
    {
      title: "Sucré Lovers",
      image: Presentes,
      active: false
    },
  ];
  const products = [
    {
      name: "Brigadeiro belga",
      price: 100.00
    },
    {
      name: "Brigadeiro belga",
      price: 100.00
    },
    {
      name: "Brigadeiro belga",
      price: 100.00
    },
    {
      name: "Brigadeiro belga",
      price: 100.00
    },
    {
      name: "Brigadeiro belga",
      price: 100.00
    },
    {
      name: "Brigadeiro belga",
      price: 100.00
    },
    {
      name: "Brigadeiro belga",
      price: 100.00
    },
    {
      name: "Brigadeiro belga",
      price: 100.00
    },
    {
      name: "Brigadeiro belga",
      price: 100.00
    },
  ]
  
  return (
    <S.Container>
      <S.Logo source={Logo} />

        <S.ScrollCategory 
          horizontal={true} 
          showsHorizontalScrollIndicator={false}
        >
          <S.Categorys>
          {categories.map((category, i) => (
            <S.CategoryCard key={i}>
              <S.CategoryImage source={category.image} />
              <S.CategoryName>{category.title}</S.CategoryName>
              {category.active ? <S.BottomBar /> : null}
            </S.CategoryCard>
          ))}
          </S.Categorys>
        </S.ScrollCategory>

      <S.Content>
        <S.Header>
          <S.BackButtonArea>
            <S.ArrowLeft source={ArrowLeft} />
          </S.BackButtonArea>
          <S.TitlePedido>Doces</S.TitlePedido>
        </S.Header>

        <S.OptionsArea>
          {products.map((product, i) => (
            <S.CardProduct key={i}>
              <S.CardImageProduct source={Brigadeiro}/>
              <S.CardNameProduct>{product.name}</S.CardNameProduct>
              <S.CardPriceProduct>R$ {product.price}</S.CardPriceProduct>
            </S.CardProduct>
          ))}
        </S.OptionsArea>
      </S.Content>
    </S.Container>
  );
};
