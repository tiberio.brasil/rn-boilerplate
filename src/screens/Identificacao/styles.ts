import { RFValue } from 'react-native-responsive-fontsize';
import { LinearGradient } from 'expo-linear-gradient';
import styled from 'styled-components/native';
import LogoImage from '../../assets/svg/logo.svg';
import BackPrimaryImage from '../../assets/svg/back-primary.svg';
import ApagarUmImage from '../../assets/svg/apagar-um.svg';
import ApagarTodosImage from '../../assets/svg/apagar-todos.svg';
import { Input } from '../../components/Input';

export const LogoImg = styled(LogoImage).attrs(() => ({
  height: 48,
}))`
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const BackPrimaryImg = styled(BackPrimaryImage).attrs(() => ({
  height: 32,
}))``;

export const ApagarUmImg = styled(ApagarUmImage).attrs(() => ({
  height: 24,
}))``;

export const ApagarTodosImg = styled(ApagarTodosImage).attrs(() => ({
  height: 32,
}))``;

export const Container = styled(LinearGradient)`
  flex: 1;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`;

export const Content = styled.View`
  flex: 1;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.background};
  padding: 28px 16px 0;
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
`;

export const Logo = styled.Image`
  margin: 30px;
`;

export const Header = styled.View`
  width: 100%;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;

export const HeaderIcons = styled.View`
  width: 40px;
`;

export const HeaderIcon = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  border-radius: 24px;
  margin: 15px 10px;
`;

export const HeaderTitle = styled.Text`
  font-size: ${RFValue(32)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  text-align: center;
  flex-grow: 1;
`;

export const IdentificationContent = styled.View`
  justify-content: center;
  align-items: flex-start;
  flex: 1;
  flex-direction: row;
  margin-top: 20px;
`;

export const LeftContent = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const RightContent = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const TouchContent = styled.View`
  justify-content: center;
  align-items: center;
  flex-direction: row;
  flex-wrap: wrap;
  width: 240px;
`;

export const InputField = styled(Input)``;

export const Subtitle = styled.Text`
  font-size: ${RFValue(24)}px;
  line-height: ${RFValue(33)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.input.text};
`;

export const ButtonTouchPrimary = styled.TouchableOpacity`
  background: ${({ theme }) => theme.colors.bottomTabBar.background};
  border-radius: 50px;
  height: 64px;
  width: 64px;
  justify-content: center;
  align-items: center;
  margin: 8px;
`;

export const ButtonTouchSecondary = styled.TouchableOpacity`
  background: ${({ theme }) => theme.colors.backgroundGraySelected};
  border-radius: 50px;
  height: 64px;
  width: 64px;
  justify-content: center;
  align-items: center;
  margin: 8px;
`;

export const ButtonText = styled.Text`
  font-size: ${RFValue(24)}px;
  line-height: ${RFValue(33)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.primary};
`;

export const CPFWrapper = styled.View`
  flex-direction: row;
  border-width: 1px;
  border-color: #bdbcbc;
  border-radius: 32px;
  padding: 10px 30px;
  margin-top: 20px;
`;

export const CPFItem = styled.Text`
  font-size: ${RFValue(24)}px;
  line-height: ${RFValue(33)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.primary};
  padding: 0 2px;
  width: 18px;
  text-align: center;
`;

export const ButtonsWrapper = styled.View`
  margin-top: 40px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const ButtonCancelContent = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.input.placeholder};
  padding: 10px 20px;
  margin: 0 10px;
  border-radius: 50px;
  width: 40%;
`;

export const ButtonCancelText = styled.Text`
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(16)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.black};
  text-align: center;
`;

export const ButtonConfirmContent = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  padding: 10px 20px;
  margin: 0 10px;
  border-radius: 50px;
  width: 40%;
`;

export const ButtonConfirmText = styled.Text`
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(16)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.white};
  text-align: center;
`;

export const ErroCpfMessage = styled.Text`
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(16)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.primary};
  text-align: center;
  margin-top: 20px;
`;
