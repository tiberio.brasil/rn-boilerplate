import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import {
  getAsyncStorage,
  multiRemoveAsyncStorage,
  setAsyncStorage,
} from '../../utils/asyncStorage';
import { EnumTipoPedido } from '../../utils/constants';
import { isValidCPF } from '../../utils/validations';
import * as S from './styles';

export const Identificacao = () => {
  const [cpf, setCpf] = useState('___________');
  const [erroCpf, setErroCpf] = useState(false);

  const navigation = useNavigation();

  // Verifica se o usuario possui as variaveis necessarias ate essa pagina
  async function checkUserSelections() {
    const loja = await getAsyncStorage('loja');
    if (!loja) {
      navigation.navigate('Loja');
    }

    const tipoPedido: EnumTipoPedido = await getAsyncStorage('tipoPedido');
    if (!tipoPedido) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Local');
    }
  }

  useEffect(() => {
    checkUserSelections();
  }, []);

  async function handleAddNumberToCPF(
    number: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
  ) {
    setCpf(cpf.replace('_', number.toString()));
  }

  async function handleDeleteOneCPFNumber() {
    let pos = -1;

    for (let i = 10; i >= 0; i--) {
      if (cpf[i] !== '_') {
        pos = i;
        break;
      }
    }

    if (pos > -1) {
      setCpf(cpf.substring(0, pos) + '_' + cpf.substr(pos + 1));
    }
  }

  async function handleDeleteAllCPFNumber() {
    setCpf('___________');
  }

  async function validaCpf() {
    if (!isValidCPF(cpf)) {
      setErroCpf(true);
    }
  }

  useEffect(() => {
    setErroCpf(false);
    if (!cpf.includes('_')) {
      validaCpf();
    }
  }, [cpf]);

  async function handleBackToFidelidade() {
    navigation.navigate('Fidelidade');
  }

  async function handleCheckCpf() {
    if (isValidCPF(cpf)) {
      await setAsyncStorage('cpf', cpf);
      navigation.navigate('Categorias');
    } else {
      setErroCpf(true);
    }
  }

  return (
    <S.Container colors={['#FA2458', '#FC4A4E', '#FED278']} start={[0.8, 0.3]}>
      <S.LogoImg />

      <S.Content>
        <S.Header>
          <S.HeaderIcons>
            <S.HeaderIcon onPress={() => handleBackToFidelidade()}>
              <S.BackPrimaryImg />
            </S.HeaderIcon>
          </S.HeaderIcons>

          <S.HeaderTitle>Clube de fidelidade</S.HeaderTitle>

          <S.HeaderIcons />
        </S.Header>

        <S.IdentificationContent>
          <S.LeftContent>
            <S.Subtitle>Digite o seu CPF</S.Subtitle>
            <S.CPFWrapper>
              <S.CPFItem>{cpf[0]}</S.CPFItem>
              <S.CPFItem>{cpf[1]}</S.CPFItem>
              <S.CPFItem>{cpf[2]}</S.CPFItem>
              <S.CPFItem>.</S.CPFItem>
              <S.CPFItem>{cpf[3]}</S.CPFItem>
              <S.CPFItem>{cpf[4]}</S.CPFItem>
              <S.CPFItem>{cpf[5]}</S.CPFItem>
              <S.CPFItem>.</S.CPFItem>
              <S.CPFItem>{cpf[6]}</S.CPFItem>
              <S.CPFItem>{cpf[7]}</S.CPFItem>
              <S.CPFItem>{cpf[8]}</S.CPFItem>
              <S.CPFItem>-</S.CPFItem>
              <S.CPFItem>{cpf[9]}</S.CPFItem>
              <S.CPFItem>{cpf[10]}</S.CPFItem>
            </S.CPFWrapper>

            {erroCpf && (
              <S.ErroCpfMessage>
                {cpf.includes('_')
                  ? 'Por favor, insira o seu CPF completo.'
                  : `CPF inválido.${'\n'}Verifique o seu CPF e tente novamente.`}
              </S.ErroCpfMessage>
            )}

            <S.ButtonsWrapper>
              <S.ButtonCancelContent onPress={() => handleBackToFidelidade()}>
                <S.ButtonCancelText>Cancelar</S.ButtonCancelText>
              </S.ButtonCancelContent>

              <S.ButtonConfirmContent onPress={() => handleCheckCpf()}>
                <S.ButtonConfirmText>Confirmar</S.ButtonConfirmText>
              </S.ButtonConfirmContent>
            </S.ButtonsWrapper>
          </S.LeftContent>
          <S.RightContent>
            <S.TouchContent>
              <S.ButtonTouchPrimary onPress={() => handleAddNumberToCPF(1)}>
                <S.ButtonText>1</S.ButtonText>
              </S.ButtonTouchPrimary>

              <S.ButtonTouchPrimary onPress={() => handleAddNumberToCPF(2)}>
                <S.ButtonText>2</S.ButtonText>
              </S.ButtonTouchPrimary>

              <S.ButtonTouchPrimary onPress={() => handleAddNumberToCPF(3)}>
                <S.ButtonText>3</S.ButtonText>
              </S.ButtonTouchPrimary>

              <S.ButtonTouchPrimary onPress={() => handleAddNumberToCPF(4)}>
                <S.ButtonText>4</S.ButtonText>
              </S.ButtonTouchPrimary>

              <S.ButtonTouchPrimary onPress={() => handleAddNumberToCPF(5)}>
                <S.ButtonText>5</S.ButtonText>
              </S.ButtonTouchPrimary>

              <S.ButtonTouchPrimary onPress={() => handleAddNumberToCPF(6)}>
                <S.ButtonText>6</S.ButtonText>
              </S.ButtonTouchPrimary>

              <S.ButtonTouchPrimary onPress={() => handleAddNumberToCPF(7)}>
                <S.ButtonText>7</S.ButtonText>
              </S.ButtonTouchPrimary>

              <S.ButtonTouchPrimary onPress={() => handleAddNumberToCPF(8)}>
                <S.ButtonText>8</S.ButtonText>
              </S.ButtonTouchPrimary>

              <S.ButtonTouchPrimary onPress={() => handleAddNumberToCPF(9)}>
                <S.ButtonText>9</S.ButtonText>
              </S.ButtonTouchPrimary>

              <S.ButtonTouchSecondary
                onPress={() => handleDeleteAllCPFNumber()}
              >
                <S.ApagarTodosImg />
              </S.ButtonTouchSecondary>

              <S.ButtonTouchPrimary onPress={() => handleAddNumberToCPF(0)}>
                <S.ButtonText>0</S.ButtonText>
              </S.ButtonTouchPrimary>

              <S.ButtonTouchSecondary
                onPress={() => handleDeleteOneCPFNumber()}
              >
                <S.ApagarUmImg />
              </S.ButtonTouchSecondary>
            </S.TouchContent>
          </S.RightContent>
        </S.IdentificationContent>
      </S.Content>
    </S.Container>
  );
};
