import { useNavigation } from '@react-navigation/native';
import React, { useEffect } from 'react';
import { getAsyncStorage, setAsyncStorage } from '../../utils/asyncStorage';
import { EnumTipoPedido } from '../../utils/constants';
import * as S from './styles';

export const Local = () => {
  const navigation = useNavigation();

  async function handleSelectOption(value: EnumTipoPedido) {
    await setAsyncStorage('tipoPedido', value);
    navigation.navigate('Fidelidade');
  }

  // Verifica se o usuario possui as variaveis necessarias ate essa pagina
  async function checkUserSelections() {
    const loja = await getAsyncStorage('loja');
    if (!loja) {
      navigation.navigate('Loja');
    }
  }

  useEffect(() => {
    checkUserSelections();
  }, []);

  return (
    <S.Container colors={['#FA2458', '#FC4A4E', '#FED278']} start={[0.8, 0.3]}>
      <S.LogoImg />

      <S.QuestionText>O que você deseja fazer?</S.QuestionText>

      <S.ButtonsOptions
        onPress={() => handleSelectOption(EnumTipoPedido.comerAqui)}
      >
        <S.ButtonOptionsText>COMER AQUI</S.ButtonOptionsText>
        <S.ComerAquiImg />
      </S.ButtonsOptions>

      <S.ButtonsOptions
        onPress={() => handleSelectOption(EnumTipoPedido.vouLevar)}
      >
        <S.ButtonOptionsText>VOU LEVAR</S.ButtonOptionsText>
        <S.VouLevarImg />
      </S.ButtonsOptions>
    </S.Container>
  );
};
