import styled from "styled-components/native";

export const Container = styled.View`
  flex: 1;
  height: 100%;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

export const Content = styled.View`
  height: 100%;
  background-color: ${({ theme }) => theme.colors.white};
  padding: 28px 16px 0;
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
`;

export const ImageLogo = styled.Image`
  width: 437px;
  height: 235px;
`;

export const Title = styled.Text`
  font-family: ${({ theme }) => theme.fonts.alternative.bold};
  color: ${({ theme }) => theme.colors.background};
  font-size: 48px;
`;

export const Background = styled.ImageBackground`
  flex: 1;
  height: 100%;
  justify-content: center;
  align-items: center;
  width: 100%;
`;