import React from "react";
import * as S from "./styles";

import Logo from "../../assets/image/logo.png";
import BackgroundT from "../../assets/image/background.png";

export const Thanks = () => {
  return (
    <S.Background source={BackgroundT}>
      <S.Title>Obrigado!</S.Title>
      <S.ImageLogo source={Logo} />
    </S.Background>
  );
};
