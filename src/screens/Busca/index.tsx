import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { IProduct } from '../../@types/interfaces';
import theme from '../../global/styles/theme';
import api from '../../services/api';
import {
  getAsyncStorage,
  multiRemoveAsyncStorage,
} from '../../utils/asyncStorage';
import { EnumFidelidade, EnumTipoPedido } from '../../utils/constants';
import { formatPrice } from '../../utils/price';
import * as S from './styles';

export const Busca = () => {
  const [loading, setLoading] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [branchId, setBranchId] = useState('');
  const [results, setResults] = useState([] as IProduct[]);

  const navigation = useNavigation();

  async function handleBackClick() {
    navigation.navigate('Categorias');
  }

  // Verifica se o usuario possui as variaveis necessarias ate essa pagina
  async function checkUserSelections() {
    const loja = await getAsyncStorage('loja');
    if (!loja) {
      navigation.navigate('Loja');
    }
    setBranchId(loja);

    const tipoPedido: EnumTipoPedido = await getAsyncStorage('tipoPedido');
    if (!tipoPedido) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Local');
    }

    const fidelidade: EnumFidelidade = await getAsyncStorage('fidelidade');
    if (!fidelidade) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Fidelidade');
    }
  }

  useEffect(() => {
    checkUserSelections();
  }, []);

  async function getProductsBySearch() {
    setLoading(true);

    if (searchTerm.trim().length === 0) {
      setResults([]);
      setLoading(false);
      return;
    }
    const searchTermTrim = `name=${searchTerm.trim()}`;

    try {
      const url = `/products?is_active=true&branch_id=${branchId}&${searchTermTrim}`;
      console.log(url);
      const response = await api.get(url);

      setResults(response.data);
    } catch (error) {
      console.error('Error getProductsBySearch()', error);
    }
    setLoading(false);
  }

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      console.log(searchTerm);
      getProductsBySearch();
    }, 1000);

    return () => clearTimeout(delayDebounceFn);
  }, [searchTerm]);

  function handleOpenProductDetails(id: string) {
    navigation.navigate('Detalhes', { id });
  }

  return (
    <S.Container colors={['#FA2458', '#FC4A4E', '#FED278']} start={[0.8, 0.3]}>
      <S.LogoImg />
      <S.CategoryBar>
        <S.CategoryBarIcon onPress={() => handleBackClick()}>
          <S.BackImg />
        </S.CategoryBarIcon>
        <S.InputSearchView>
          <S.InputSearch
            placeholder='Pesquisar um produto...'
            onChangeText={setSearchTerm}
            value={searchTerm}
            autoFocus
            autoCorrect
          />
        </S.InputSearchView>
        <S.CategoryBarIcon>
          <S.SearchImg onPress={() => getProductsBySearch()} />
        </S.CategoryBarIcon>
      </S.CategoryBar>

      {loading ? (
        <ActivityIndicator
          size='large'
          color={theme.colors.submit.primary.color}
          style={{ marginTop: 50 }}
        />
      ) : (
        <S.SearchContainer>
          <S.SearchContent>
            {results.length > 0 ? (
              results.map((product, i) => {
                let priceFrom = null;
                let priceTo = null;

                if (product.promotions[0]?.value) {
                  priceTo = formatPrice(product.promotions[0]?.value);
                  priceFrom = formatPrice(product.product_prices[0].price);
                } else {
                  priceTo = formatPrice(product.product_prices[0].price);
                }

                // if (i > 2 && i < 13) {
                //   priceTo = formatPrice(120.9);
                //   priceFrom = formatPrice(100.9);
                // }

                return (
                  <S.ProductCard
                    key={`${product.id}-${i}`}
                    onPress={() => handleOpenProductDetails(product.id)}
                  >
                    {product.thumbnail_url ? (
                      <S.ProductImage source={{ uri: product.thumbnail_url }} />
                    ) : (
                      <S.SemImagemImg />
                    )}
                    <S.ProductTitle numberOfLines={2}>
                      {product.name}
                    </S.ProductTitle>

                    <S.ProductPrices>
                      <S.ProductPriceTo>R$ {priceTo}</S.ProductPriceTo>
                      {priceFrom && (
                        <S.ProductPriceFrom>R$ {priceFrom}</S.ProductPriceFrom>
                      )}
                    </S.ProductPrices>
                  </S.ProductCard>
                );
              })
            ) : (
              <S.SearchResultWrapper>
                <S.SearchResultImages>
                  <S.Result1Img />
                  <S.Result2Img />
                  <S.Result3Img />
                  <S.Result4Img />
                </S.SearchResultImages>
                <S.SearchResultText>
                  {searchTerm.trim() === ''
                    ? 'Insira o nome de um produto no campo de busca.'
                    : `Nenhum produto com o nome "${searchTerm}" foi encontrado. ${'\n'}Por favor, tente um outro nome.`}
                </S.SearchResultText>
              </S.SearchResultWrapper>
            )}
          </S.SearchContent>
        </S.SearchContainer>
      )}
    </S.Container>
  );
};
