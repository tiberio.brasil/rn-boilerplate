import { LinearGradient } from 'expo-linear-gradient';
import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';
import BackImage from '../../assets/svg/back.svg';
import LogoImage from '../../assets/svg/logo.svg';
import Result1Image from '../../assets/svg/resultado-1.svg';
import Result2Image from '../../assets/svg/resultado-2.svg';
import Result3Image from '../../assets/svg/resultado-3.svg';
import Result4Image from '../../assets/svg/resultado-4.svg';
import SearchImage from '../../assets/svg/search.svg';
import SemImagemImage from '../../assets/svg/sem-imagem.svg';
import { Input } from '../../components/Input';

export const LogoImg = styled(LogoImage).attrs(() => ({
  height: 48,
}))`
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const BackImg = styled(BackImage).attrs(() => ({
  height: 32,
}))``;

export const SearchImg = styled(SearchImage).attrs(() => ({
  height: 32,
}))``;

export const Result1Img = styled(Result1Image).attrs(() => ({
  height: 80,
}))``;

export const Result2Img = styled(Result2Image).attrs(() => ({
  height: 80,
}))``;

export const Result3Img = styled(Result3Image).attrs(() => ({
  height: 80,
}))``;

export const Result4Img = styled(Result4Image).attrs(() => ({
  height: 80,
}))``;

export const Container = styled(LinearGradient)`
  flex: 1;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`;

export const CategoryBar = styled.View`
  width: 100%;
  background-color: rgba(35, 31, 32, 0.17);
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

export const CategoryBarIcon = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.white};
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  border-radius: 24px;
  margin: 15px 10px;
`;

export const SearchContainer = styled.ScrollView`
  width: 100%;
  align-self: center;
  background-color: ${({ theme }) => theme.colors.white};
  border-top-left-radius: 16px;
  border-top-right-radius: 16px;
`;

export const SearchContent = styled.View`
  justify-content: center;
  align-items: stretch;
  align-self: center;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: 30px;
`;

export const CategoryCard = styled.TouchableOpacity`
  width: 18%;
  background-color: rgba(35, 31, 32, 0.17);
  align-items: center;
  justify-content: center;
  border-radius: 22px;
  margin: 10px 10px 30px;
`;

export const CategoryImage = styled.Image`
  top: -30px;
  width: 100px;
`;

export const CategoryTitle = styled.Text`
  top: -30px;
  font-family: ${({ theme }) => theme.fonts.alternative.regular};
  color: ${({ theme }) => theme.colors.title};
  font-size: ${RFValue(13)}px;
  line-height: ${RFValue(18)}px;
  text-align: center;
  height: ${RFValue(36)}px;
`;

export const SemImagemImg = styled(SemImagemImage).attrs(() => ({
  height: 96,
}))`
  top: -30px;
  width: 100px;
`;

export const InputSearchView = styled.View`
  flex-grow: 1;
`;

export const InputSearch = styled(Input)``;

export const SearchResultWrapper = styled.View`
  flex-direction: column;
  margin-top: 60px;
  justify-content: center;
  align-items: center;
  width: 80%;
`;

export const SearchResultImages = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const SearchResultText = styled.Text`
  text-align: center;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.primary};
  font-size: ${RFValue(20)}px;
  line-height: ${RFValue(26)}px;
  padding-top: 20px;
  flex-wrap: wrap;
`;

export const ProductCard = styled.TouchableOpacity`
  width: 18%;
  background-color: ${({ theme }) => theme.colors.backgroundGrayLight};
  align-items: center;
  justify-content: center;
  border-radius: 22px;
  margin: 10px 10px 30px;
`;

export const ProductImage = styled.Image.attrs(() => ({
  height: 100,
  width: 100,
}))`
  top: -30px;
  border-radius: 16px;
  height: 100px;
  width: 100px;
`;

export const ProductTitle = styled.Text`
  top: -30px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.black};
  font-size: ${RFValue(13)}px;
  line-height: ${RFValue(18)}px;
  text-align: center;
  height: ${RFValue(36)}px;
`;

export const ProductPrices = styled.View`
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const ProductPriceTo = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: ${RFValue(18)}px;
  line-height: ${RFValue(22)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  top: -20px;
  text-align: center;
`;

export const ProductPriceFrom = styled.Text`
  color: ${({ theme }) => theme.colors.textLight};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(18)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  top: -20px;
  text-align: center;
  text-decoration: line-through;
`;
