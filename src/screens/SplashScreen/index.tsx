import { useNavigation } from '@react-navigation/native';
import { useEffect } from 'react';
import { getAsyncStorage } from '../../utils/asyncStorage';
import * as S from './styles';

export const SplashScreen = () => {
  const navigation = useNavigation();

  function handleOpenLocalScreen() {
    navigation.navigate('Local');
  }

  // Verifica se o usuario possui as variaveis necessarias ate essa pagina
  async function checkUserSelections() {
    const loja = await getAsyncStorage('loja');
    if (!loja) {
      navigation.navigate('Loja');
    }
  }

  useEffect(() => {
    checkUserSelections();
  }, []);

  return (
    <S.Container onPress={handleOpenLocalScreen}>
      <S.ContainerWrapper
        colors={['#FA2458', '#FC4A4E', '#FED278']}
        start={[0.8, 0.3]}
      >
        <S.BackgroundImg />
        <S.LogoImg />
        <S.MessageWrapper>
          <S.LeftImg />
          <S.Message>TOQUE NA TELA PARA COMEÇAR</S.Message>
          <S.RightImg />
        </S.MessageWrapper>
      </S.ContainerWrapper>
    </S.Container>
  );
};
