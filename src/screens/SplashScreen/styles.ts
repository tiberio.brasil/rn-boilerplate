import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';
import { LinearGradient } from 'expo-linear-gradient';
import BackgroundImage from '../../assets/svg/background.svg';
import LeftImage from '../../assets/svg/left.svg';
import LogoImage from '../../assets/svg/logo.svg';
import RightImage from '../../assets/svg/right.svg';

export const LogoImg = styled(LogoImage).attrs(() => ({
  height: 180,
}))``;

export const BackgroundImg = styled(BackgroundImage).attrs(() => ({
  width: '100%',
  resizeMode: 'repeat',
}))`
  position: absolute;
`;

export const LeftImg = styled(LeftImage).attrs(() => ({
  height: 40,
}))`
  margin-right: 20px;
`;

export const RightImg = styled(RightImage).attrs(() => ({
  height: 40,
}))`
  margin-left: 20px;
`;

export const Container = styled.TouchableOpacity`
  flex: 1;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

export const ContainerWrapper = styled(LinearGradient)`
  flex: 1;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

export const MessageWrapper = styled.View`
  position: absolute;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  bottom: 30px;
`;

export const Message = styled.Text`
  font-family: ${({ theme }) => theme.fonts.alternative.bold};
  color: ${({ theme }) => theme.colors.background};
  font-size: ${RFValue(20)}px;
  line-height: ${RFValue(41)}px;
`;
