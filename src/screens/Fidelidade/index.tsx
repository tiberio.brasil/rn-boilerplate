import { useNavigation } from '@react-navigation/native';
import React, { useEffect } from 'react';
import {
  getAsyncStorage,
  multiRemoveAsyncStorage,
  setAsyncStorage,
} from '../../utils/asyncStorage';
import { EnumFidelidade, EnumTipoPedido } from '../../utils/constants';
import * as S from './styles';

export const Fidelidade = () => {
  const navigation = useNavigation();

  async function handleSelectOption(value: EnumFidelidade) {
    await setAsyncStorage('fidelidade', value);
    if (value === EnumFidelidade.sim) {
      navigation.navigate('Identificacao');
    } else {
      await setAsyncStorage('cpf', '');
      navigation.navigate('Categorias');
    }
  }

  // Verifica se o usuario possui as variaveis necessarias ate essa pagina
  async function checkUserSelections() {
    const loja = await getAsyncStorage('loja');
    if (!loja) {
      navigation.navigate('Loja');
    }

    const tipoPedido: EnumTipoPedido = await getAsyncStorage('tipoPedido');
    if (!tipoPedido) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Local');
    }
  }

  useEffect(() => {
    checkUserSelections();
  }, []);

  return (
    <S.Container colors={['#FA2458', '#FC4A4E', '#FED278']} start={[0.8, 0.3]}>
      <S.LogoImg />

      <S.QuestionText>
        Deseja comprar com seus pontos fidelidade?
      </S.QuestionText>

      <S.ButtonsOptions onPress={() => handleSelectOption(EnumFidelidade.sim)}>
        <S.ButtonOptionsText>QUERO SIM</S.ButtonOptionsText>
        <S.QueroSimImg />
      </S.ButtonsOptions>

      <S.ButtonsOptions onPress={() => handleSelectOption(EnumFidelidade.nao)}>
        <S.ButtonOptionsText>AGORA NÃO</S.ButtonOptionsText>
        <S.AgoraNaoImg />
      </S.ButtonsOptions>
    </S.Container>
  );
};
