import { LinearGradient } from 'expo-linear-gradient';
import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';
import AgoraNaoImage from '../../assets/svg/agora-nao.svg';
import LogoImage from '../../assets/svg/logo.svg';
import QueroSimImage from '../../assets/svg/quero-sim.svg';

export const LogoImg = styled(LogoImage).attrs(() => ({
  width: '26%',
  height: 180,
}))``;

export const QueroSimImg = styled(QueroSimImage)`
  margin-left: 20px;
`;

export const AgoraNaoImg = styled(AgoraNaoImage)`
  margin-left: 20px;
`;

export const Container = styled(LinearGradient)`
  flex: 1;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`;

export const ButtonsOptions = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.white};
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 15px 60px;
  border-radius: 49px;
  margin-top: 30px;
  width: 60%;
`;

export const ButtonOptionsText = styled.Text`
  font-size: ${RFValue(30)}px;
  line-height: ${RFValue(41)}px;
  color: ${({ theme }) => theme.colors.primary};
  font-family: ${({ theme }) => theme.fonts.alternative.bold};
`;

export const QuestionText = styled.Text`
  font-size: ${RFValue(30)}px;
  line-height: ${RFValue(49)}px;
  color: ${({ theme }) => theme.colors.background};
  font-family: ${({ theme }) => theme.fonts.regular};
`;
