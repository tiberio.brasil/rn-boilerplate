import styled from "styled-components/native";

export const Container = styled.View`
  flex: 1;
  height: 100%;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const Content = styled.View`
  flex: 1;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.white};
  padding: 28px 16px 0;
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
`;

export const Logo = styled.Image`
  margin: 30px;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
`;

export const BackButtonArea = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  border-radius: 99px;
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px;
`;

export const TitlePedido = styled.Text`
  font-size: 32px;
  font-family: ${({ theme }) => theme.fonts.regular};
  text-align: center;
  width: 90%;
`;

export const ArrowLeft = styled.Image``;
export const ImageProduct = styled.Image`
  width: 43px;
  height: 43px;
`;

export const ProductArea = styled.ScrollView`
  flex: 1;
  margin-top: 20px;
`;

export const NameProduct = styled.Text`
  font-size: 18px;
  width: 60%;
  font-family: ${({ theme }) => theme.fonts.regular};
`;

export const TextNumber = styled.Text`
  font-size: 20px;
  font-family: ${({ theme }) => theme.fonts.regular};
`;

export const TextQuantity = styled.Text`
  font-size: 20px;
  font-family: ${({ theme }) => theme.fonts.regular};
`;

export const ButtonConfirm = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  border-radius: 49px;
  width: 350px;
  height: 72px;
  align-items: center;
  justify-content: center;
  margin-top: 40px;
  align-self: center;
`;

export const ButtonConfirmText = styled.Text`
  color: ${({ theme }) => theme.colors.white};
  font-size: 22px;
  font-family: ${({ theme }) => theme.fonts.regular};
  margin-left: 16px;
`;

export const DetailsSingle = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;

export const DetailsSingleLeft = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  width: 80%;
  padding: 10px;
  align-items: center;
`;

export const ContainerDetails = styled.View`
  background-color: ${({ theme }) => theme.colors.backgroundGrayLight};
  border-radius: 16px;
  padding: 20px;
  width: 60%;
  border-width: 1px;
  border-color: ${({ theme }) => theme.colors.border};
  align-self: center;
`;

export const Title = styled.Text`
  font-size: 24px;
  text-align: center;
  font-family: ${({ theme }) => theme.fonts.regular};
  margin-top: 25px;
`;

export const Description = styled.Text`
  font-size: 20px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.input.text};
  padding: 25px;
`;

export const ButtonCupons = styled.View`
  background-color: ${({ theme }) => theme.colors.background};
  border-radius: 49px;
  align-items: center;
  justify-content: center;
  width: 90%;
  padding: 20px;
  align-self: center;
  margin-top: 10px;
`;

export const InputCupom = styled.TextInput`
  font-size: 18px;
  font-family: ${({ theme }) => theme.fonts.regular};
`;

export const TotalText = styled.Text`
  font-size: 18px;
  font-family: ${({ theme }) => theme.fonts.regular};
  text-align: center;
  margin-top: 20px;
`;

export const PriceText = styled.Text`
  font-size: 32px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.primary};
`;

export const ContainerButtonsBottom = styled.View`
  flex-direction: row;
  align-self: center;
  margin-top: 30px;
`;

export const TitleButton = styled.Text`
  font-size: 22px;
  font-family: ${({ theme }) => theme.fonts.regular};
`;

export const CancelButton = styled.TouchableOpacity`
  border-radius: 50px;
  align-items: center;
  justify-content: center;
  height: 72px;
  width: 350px;
  background-color: ${({ theme }) => theme.colors.textLight};
`;

export const ReturnMenu = styled.TouchableOpacity`
  border-radius: 50px;
  align-items: center;
  justify-content: center;
  height: 72px;
  width: 350px;
  background-color: ${({ theme }) => theme.colors.bottomTabBar.background};
`;