import React from "react";
import * as S from "./styles";

import Logo from "../../assets/image/logo.png";
import ArrowLeft from "../../assets/icons/arrow-left-white.png";
import Brigadeiro from "../../assets/image/brigadeiro.png";

export const DetailsProduct = () => {
  const products = [
    {
      quantity: 1,
      nameProduct: "BRIGADEIRO BELGA",
      price: 2.99
    },
    {
      quantity: 3,
      nameProduct: "BRIGADEIRO ALEMÃO",
      price: 2.99
    },
    {
      quantity: 1,
      nameProduct: "COXINHA DE FRANGO",
      price: 2.99
    },
    {
      quantity: 5,
      nameProduct: "COXINHA DE BACALHAU",
      price: 2.99
    },
  ]
  
  return (
    <S.Container>
      <S.Logo source={Logo} />

      <S.Content>
        <S.Header>
          <S.BackButtonArea>
            <S.ArrowLeft source={ArrowLeft} />
          </S.BackButtonArea>
          <S.TitlePedido>Seu pedido</S.TitlePedido>
        </S.Header>

        <S.ProductArea>
          
          <S.ContainerDetails>
            {products.map((product, i) => (
              <S.DetailsSingle key={i}>
                <S.DetailsSingleLeft>
                  <S.ImageProduct source={Brigadeiro} />
                  <S.TextQuantity>{product.quantity}x</S.TextQuantity>
                  <S.NameProduct>{product.nameProduct}</S.NameProduct>
                </S.DetailsSingleLeft>
                <S.TextNumber>R$ {product.price}</S.TextNumber>
              </S.DetailsSingle>
            ))}

            <S.Title>Observações</S.Title>
            <S.Description>Refrigerante com gelo</S.Description>

            <S.Title>Cupons</S.Title>
            <S.ButtonCupons>
              <S.InputCupom
                placeholder="Inserir um cupom"
              />
            </S.ButtonCupons>

            <S.TotalText>
              Total
              <S.PriceText>  R$ 0,00</S.PriceText>
            </S.TotalText>
          </S.ContainerDetails>


          <S.ButtonConfirm>
            <S.ButtonConfirmText>Confirmar pedido</S.ButtonConfirmText>
          </S.ButtonConfirm>
          <S.ContainerButtonsBottom>
            <S.CancelButton>
              <S.TitleButton>Cancelar</S.TitleButton>
            </S.CancelButton>

            <S.ReturnMenu>
              <S.TitleButton>Voltar para o Cardápio</S.TitleButton>
            </S.ReturnMenu>
          </S.ContainerButtonsBottom>
        </S.ProductArea>

      </S.Content>
    </S.Container>
  );
};
