import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { ICategory } from '../../@types/interfaces';
import theme from '../../global/styles/theme';
import api from '../../services/api';
import {
  getAsyncStorage,
  multiRemoveAsyncStorage,
} from '../../utils/asyncStorage';
import { EnumFidelidade, EnumTipoPedido } from '../../utils/constants';
import * as S from './styles';

export const Categorias = () => {
  const [loading, setLoading] = useState(false);
  const [categories, setCategories] = useState<ICategory[]>([]);

  const navigation = useNavigation();

  async function handleBackClick() {
    await multiRemoveAsyncStorage();
    navigation.navigate('Local');
  }

  async function handleSearchClick() {
    navigation.navigate('Busca');
  }

  // Verifica se o usuario possui as variaveis necessarias ate essa pagina
  async function checkUserSelections() {
    const loja = await getAsyncStorage('loja');
    if (!loja) {
      navigation.navigate('Loja');
    }

    const tipoPedido: EnumTipoPedido = await getAsyncStorage('tipoPedido');
    if (!tipoPedido) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Local');
    }

    const fidelidade: EnumFidelidade = await getAsyncStorage('fidelidade');
    if (!fidelidade) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Fidelidade');
    }
  }

  useEffect(() => {
    checkUserSelections();
  }, []);

  async function getCategories() {
    setLoading(true);
    try {
      const response = await api.get(`/categories`);
      setCategories(response.data);
    } catch (error) {
      console.error('Error Categories()', error);
    }

    console.log('-------- INI --------');
    const loja = await getAsyncStorage('loja');
    console.log('loja', loja);
    const tipoPedido = await getAsyncStorage('tipoPedido');
    console.log('tipoPedido', tipoPedido);
    const fidelidade = await getAsyncStorage('fidelidade');
    console.log('fidelidade', fidelidade);
    const cpf = await getAsyncStorage('cpf');
    console.log('cpf', cpf);
    console.log('-------- FIM --------');

    setLoading(false);
  }

  useEffect(() => {
    getCategories();
  }, []);

  function handleOpenCategory(id: number) {
    navigation.navigate('CategoriaLista', { id });
  }

  return (
    <S.Container colors={['#FA2458', '#FC4A4E', '#FED278']} start={[0.8, 0.3]}>
      <S.LogoImg />
      <S.Header>
        <S.HeaderIcon onPress={() => handleBackClick()}>
          <S.BackImg />
        </S.HeaderIcon>
        <S.HeaderTitle>CATEGORIAS</S.HeaderTitle>
        <S.HeaderIcon onPress={() => handleSearchClick()}>
          <S.SearchImg />
        </S.HeaderIcon>
      </S.Header>

      {loading ? (
        <ActivityIndicator
          size='large'
          color={theme.colors.submit.primary.color}
          style={{ marginTop: 50 }}
        />
      ) : (
        <S.CategoryContainer>
          <S.CategoryContent>
            {categories.length > 0 &&
              categories.map((category) => (
                <S.CategoryCard
                  key={category.number}
                  onPress={() => handleOpenCategory(category.number)}
                >
                  {category.thumbnail_url ? (
                    <S.CategoryImage source={{ uri: category.thumbnail_url }} />
                  ) : (
                    <S.SemImagemImg />
                  )}
                  <S.CategoryTitle numberOfLines={2}>
                    {category.name}
                  </S.CategoryTitle>
                </S.CategoryCard>
              ))}
          </S.CategoryContent>
        </S.CategoryContainer>
      )}
    </S.Container>
  );
};
