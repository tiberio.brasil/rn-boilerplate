import { RFValue } from 'react-native-responsive-fontsize';
import { LinearGradient } from 'expo-linear-gradient';
import styled from 'styled-components/native';
import LogoImage from '../../assets/svg/logo.svg';
import BackImage from '../../assets/svg/back.svg';
import SearchImage from '../../assets/svg/search.svg';
import SemImagemImage from '../../assets/svg/sem-imagem.svg';

export const LogoImg = styled(LogoImage).attrs(() => ({
  height: 48,
}))`
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const BackImg = styled(BackImage).attrs(() => ({
  height: 32,
}))``;

export const SearchImg = styled(SearchImage).attrs(() => ({
  height: 32,
}))``;

export const Container = styled(LinearGradient)`
  flex: 1;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`;

export const Header = styled.View`
  width: 100%;
  background-color: rgba(35, 31, 32, 0.17);
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;

export const HeaderIcon = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.white};
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  border-radius: 24px;
  margin: 15px 10px;
`;

export const Icon = styled.Image``;

export const HeaderTitle = styled.Text`
  font-size: ${RFValue(20)}px;
  line-height: ${RFValue(24)}px;
  font-family: ${({ theme }) => theme.fonts.alternative.bold};
  color: ${({ theme }) => theme.colors.title};
`;

export const CategoryContainer = styled.ScrollView`
  width: 100%;
  align-self: center;
  margin-top: 30px;
`;

export const CategoryContent = styled.View`
  justify-content: center;
  align-items: stretch;
  align-self: center;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: 30px;
`;

export const CategoryCard = styled.TouchableOpacity`
  width: 18%;
  background-color: rgba(35, 31, 32, 0.17);
  align-items: center;
  justify-content: center;
  border-radius: 22px;
  margin: 10px 10px 30px;
`;

export const CategoryImage = styled.Image.attrs(() => ({
  height: 100,
  width: 100,
}))`
  top: -30px;
  border-radius: 16px;
  height: 100px;
  width: 100px;
`;

export const CategoryTitle = styled.Text`
  top: -30px;
  font-family: ${({ theme }) => theme.fonts.alternative.regular};
  color: ${({ theme }) => theme.colors.title};
  font-size: ${RFValue(13)}px;
  line-height: ${RFValue(18)}px;
  text-align: center;
  height: ${RFValue(36)}px;
`;

export const SemImagemImg = styled(SemImagemImage).attrs(() => ({
  height: 96,
}))`
  top: -30px;
  width: 100px;
`;
