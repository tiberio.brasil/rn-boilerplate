import { useNavigation, useRoute } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { IProduct } from '../../@types/interfaces';
import Add from '../../assets/icons/add.png';
import Car from '../../assets/icons/car.png';
import Mine from '../../assets/icons/mine.png';
import theme from '../../global/styles/theme';
import api from '../../services/api';
import {
  getAsyncStorage,
  multiRemoveAsyncStorage,
} from '../../utils/asyncStorage';
import { addItemToCart } from '../../utils/cart';
import { EnumFidelidade, EnumTipoPedido } from '../../utils/constants';
import { formatPrice } from '../../utils/price';
import * as S from './styles';

type ParamsProps = {
  id: string;
};

export const Carrinho = () => {
  const [loading, setLoading] = useState(false);
  const [branchId, setBranchId] = useState('');
  const [quantity, setQuantity] = useState(1);
  const [product, setProduct] = useState<IProduct>({} as IProduct);
  const [priceTo, setPriceTo] = useState<number | null>(null);
  const [priceFrom, setPriceFrom] = useState<number | null>(null);

  const navigation = useNavigation();
  const route = useRoute();

  function handleBackClick() {
    if (navigation.canGoBack()) {
      navigation.goBack();
    } else {
      navigation.navigate('Categorias');
    }
  }

  // Verifica se o usuario possui as variaveis necessarias ate essa pagina
  async function checkUserSelections() {
    const loja = await getAsyncStorage('loja');
    if (!loja) {
      navigation.navigate('Loja');
    }
    setBranchId(loja);

    const tipoPedido: EnumTipoPedido = await getAsyncStorage('tipoPedido');
    if (!tipoPedido) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Local');
    }

    const fidelidade: EnumFidelidade = await getAsyncStorage('fidelidade');
    if (!fidelidade) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Fidelidade');
    }
  }

  async function getProduct() {
    setLoading(true);

    try {
      const { id: productId } = route.params as ParamsProps;

      const response = await api.get(
        `/products/${productId}?branch_id=${branchId}`
      );
      const productInfo = response.data;

      if (!productInfo.id) {
        navigation.navigate('Categorias');
        return;
      }

      if (productInfo.promotions[0]?.value) {
        setPriceTo(
          productInfo.promotions[0]?.value ||
            productInfo.product_prices[0].price!
        );
        setPriceFrom(productInfo.product_prices[0].price);
      } else {
        setPriceTo(productInfo.product_prices[0].price);
        setPriceFrom(null);
      }

      // setPriceTo(1240.89);
      // setPriceFrom(1100.87);

      setProduct(productInfo);
    } catch (error) {
      console.error('Error getProduct()', error);
    }

    setLoading(false);
  }

  useEffect(() => {
    checkUserSelections();
    getProduct();
  }, []);

  function handleIncreaseProductQuantity() {
    console.log('clicou handleAddProductToCart()');
    setQuantity((old) => old + 1);
  }

  function handleDecreaseProductQuantity() {
    console.log('clicou handleRemoveProductFromCart()');
    setQuantity((old) => (old > 1 ? old - 1 : 1));
  }

  async function handleAddProductToCart() {
    await addItemToCart(product.id, quantity);
  }

  return (
    <S.Container colors={['#FA2458', '#FC4A4E', '#FED278']} start={[0.8, 0.3]}>
      <S.LogoImg />

      <S.Content>
        <S.Header>
          <S.HeaderIcons>
            <S.HeaderIcon onPress={() => handleBackClick()}>
              <S.BackPrimaryImg />
            </S.HeaderIcon>
          </S.HeaderIcons>

          <S.HeaderTitle>{product.name}</S.HeaderTitle>

          <S.HeaderIcons />
        </S.Header>

        {loading ? (
          <ActivityIndicator
            size='large'
            color={theme.colors.submit.primary.color}
            style={{ marginTop: 50 }}
          />
        ) : (
          <S.ProductWrapper>
            <S.ProductInfo>
              {product.thumbnail_url ? (
                <S.ProductImage source={{ uri: product.thumbnail_url }} />
              ) : (
                <S.SemImagemProductImg />
              )}
              <S.ProductDetails numberOfLines={2}>
                {product.description}
              </S.ProductDetails>
              <S.ProductPrices>
                {priceTo && priceTo > 0 && (
                  <S.ProductPriceTo>R$ {formatPrice(priceTo)}</S.ProductPriceTo>
                )}
                {priceFrom && priceFrom > 0 && (
                  <S.ProductPriceFrom>
                    R$ {formatPrice(priceFrom)}
                  </S.ProductPriceFrom>
                )}
              </S.ProductPrices>
            </S.ProductInfo>

            <S.CartInfo>
              <S.QuantityText>Quantidade</S.QuantityText>

              <S.QuantityArea>
                <S.QuantityContent
                  onPress={() => handleDecreaseProductQuantity()}
                  style={{ opacity: quantity === 1 ? 0.2 : 1 }}
                  disabled={quantity === 1}
                >
                  <S.MenosImg />
                </S.QuantityContent>

                <S.QuantityTotal>{quantity}</S.QuantityTotal>

                <S.QuantityContent
                  onPress={() => handleIncreaseProductQuantity()}
                >
                  <S.MaisImg />
                </S.QuantityContent>
              </S.QuantityArea>

              {priceTo && priceTo > 0 && quantity > 0 && (
                <S.Total>
                  Total:{' '}
                  <S.TotalPrice>
                    R$ {formatPrice(quantity * priceTo)}
                  </S.TotalPrice>
                </S.Total>
              )}
              <S.ButtonAddCar
                onPress={async () => await handleAddProductToCart()}
              >
                <S.CartImg />
                <S.ButtonAddCarText>Adicionar ao carrinho</S.ButtonAddCarText>
              </S.ButtonAddCar>
            </S.CartInfo>
          </S.ProductWrapper>
        )}
      </S.Content>
    </S.Container>
  );
};
