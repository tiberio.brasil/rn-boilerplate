import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';
import { LinearGradient } from 'expo-linear-gradient';
import LogoImage from '../../assets/svg/logo.svg';
import BackPrimaryImage from '../../assets/svg/back-primary.svg';
import SemImagemImage from '../../assets/svg/sem-imagem.svg';
import MenosImage from '../../assets/svg/menos.svg';
import MaisImage from '../../assets/svg/mais.svg';
import CartImage from '../../assets/svg/cart.svg';

export const LogoImg = styled(LogoImage).attrs(() => ({
  height: 48,
}))`
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const BackPrimaryImg = styled(BackPrimaryImage).attrs(() => ({
  height: 32,
}))``;

export const MenosImg = styled(MenosImage).attrs(() => ({
  height: 32,
}))``;

export const MaisImg = styled(MaisImage).attrs(() => ({
  height: 32,
}))``;

export const CartImg = styled(CartImage).attrs(() => ({
  height: 16,
}))``;

export const Container = styled(LinearGradient)`
  flex: 1;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`;

export const Content = styled.ScrollView`
  flex: 1;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.background};
  padding: 28px 16px 0;
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
`;

export const Header = styled.View`
  width: 100%;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;

export const HeaderIcons = styled.View`
  width: 40px;
`;

export const HeaderIcon = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  border-radius: 24px;
  margin: 15px 10px;
`;

export const HeaderTitle = styled.Text`
  font-size: ${RFValue(32)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  text-align: center;
  flex-grow: 1;
`;

export const ProductWrapper = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
  margin-top: 10px;
`;

export const ProductInfo = styled.View`
  flex: 1;
  flex-direction: column;
  text-align: center;
  justify-content: flex-start;
  align-items: center;
`;

export const CartInfo = styled.View`
  flex: 1;
  flex-direction: column;
  text-align: center;
  justify-content: center;
  align-items: center;
  border-left-width: 2px;
  border-left-color: ${({ theme }) => theme.colors.input.border};
`;

export const ImageProduct = styled.Image`
  width: 255px;
  align-self: center;
  height: 255px;
`;

export const ImageQuantity = styled.Image``;

export const IconCar = styled.Image``;

export const ProductDetails = styled.Text`
  text-align: center;
  font-family: ${({ theme }) => theme.fonts.alternative.regular};
  color: ${({ theme }) => theme.colors.input.text};
  font-size: ${RFValue(15)}px;
  line-height: ${RFValue(19)}px;
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const ProductPrices = styled.View`
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const ProductPriceTo = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: ${RFValue(32)}px;
  line-height: ${RFValue(40)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  text-align: center;
`;

export const ProductPriceFrom = styled.Text`
  color: ${({ theme }) => theme.colors.textLight};
  font-size: ${RFValue(20)}px;
  line-height: ${RFValue(28)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  text-align: center;
  text-decoration: line-through;
`;

export const QuantityText = styled.Text`
  color: ${({ theme }) => theme.colors.input.text};
  font-family: ${({ theme }) => theme.fonts.regular};
  font-size: ${RFValue(24)}px;
  text-align: center;
`;

export const QuantityArea = styled.View`
  align-self: center;
  flex-direction: row;
  align-items: center;
  margin-top: 20px;
  width: 224px;
  justify-content: space-between;
`;

export const QuantityTotal = styled.Text`
  font-family: ${({ theme }) => theme.fonts.alternative.regular};
  color: ${({ theme }) => theme.colors.text};
  font-size: ${RFValue(24)}px;
  line-height: ${RFValue(33)}px;
`;

export const QuantityContent = styled.TouchableOpacity``;

export const Total = styled.Text`
  margin-top: 24px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.input.text};
  font-size: ${RFValue(22)}px;
  line-height: ${RFValue(30)}px;
  text-align: center;
`;

export const TotalPrice = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: ${RFValue(22)}px;
  line-height: ${RFValue(30)}px;
`;

export const ButtonAddCar = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  border-radius: 50px;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  margin-top: 24px;
  padding: 8px 20px;
`;

export const ButtonAddCarText = styled.Text`
  color: ${({ theme }) => theme.colors.white};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(22)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  padding-left: 8px;
`;

export const ProductImage = styled.Image.attrs(() => ({
  height: 200,
  width: 200,
}))`
  border-radius: 16px;
  height: 200px;
  width: 200px;
`;

export const SemImagemProductImg = styled(SemImagemImage).attrs(() => ({
  height: 200,
}))``;
