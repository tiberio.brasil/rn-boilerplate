import { LinearGradient } from 'expo-linear-gradient';
import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';
import BackImage from '../../assets/svg/back.svg';
import LogoImage from '../../assets/svg/logo.svg';
import Result1Image from '../../assets/svg/resultado-1.svg';
import Result2Image from '../../assets/svg/resultado-2.svg';
import Result3Image from '../../assets/svg/resultado-3.svg';
import Result4Image from '../../assets/svg/resultado-4.svg';
import SearchImage from '../../assets/svg/search.svg';
import SemImagemImage from '../../assets/svg/sem-imagem.svg';

export const LogoImg = styled(LogoImage).attrs(() => ({
  height: 48,
}))`
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const BackImg = styled(BackImage).attrs(() => ({
  height: 32,
}))``;

export const SearchImg = styled(SearchImage).attrs(() => ({
  height: 32,
}))``;

export const Result1Img = styled(Result1Image).attrs(() => ({
  height: 80,
}))``;

export const Result2Img = styled(Result2Image).attrs(() => ({
  height: 80,
}))``;

export const Result3Img = styled(Result3Image).attrs(() => ({
  height: 80,
}))``;

export const Result4Img = styled(Result4Image).attrs(() => ({
  height: 80,
}))``;

export const Container = styled(LinearGradient)`
  flex: 1;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`;

export const Header = styled.View`
  width: 100%;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;

export const HeaderIcon = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.white};
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  border-radius: 24px;
  margin: 15px 10px;
`;

export const Icon = styled.Image``;

export const InfoTitle = styled.Text`
  font-size: ${RFValue(20)}px;
  line-height: ${RFValue(28)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.black};
  text-align: center;
  width: 100%;
  padding-bottom: 40px;
`;

export const CategoryHorizontalList = styled.ScrollView`
  flex-direction: row;
`;

export const CategoryCard = styled.TouchableOpacity`
  width: 80px;
  background-color: rgba(35, 31, 32, 0.17);
  align-items: center;
  justify-content: center;
  border-radius: 22px;
  margin: 20px 10px 8px;
`;

export const CategoryImage = styled.Image.attrs(() => ({
  height: 60,
  width: 60,
}))`
  top: -15px;
  border-radius: 16px;
  height: 60px;
  width: 60px;
`;

export const SemImagemImg = styled(SemImagemImage).attrs(() => ({
  height: 60,
}))`
  top: -15px;
  width: 60px;
`;

export const CategoryTitle = styled.Text`
  top: -10px;
  font-family: ${({ theme }) => theme.fonts.alternative.regular};
  color: ${({ theme }) => theme.colors.title};
  font-size: ${RFValue(10)}px;
  line-height: ${RFValue(12)}px;
  text-align: center;
  height: ${RFValue(24)}px;
`;

export const ProductsContainer = styled.ScrollView`
  flex: 1;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.background};
  padding: 10px 0 0;
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
`;

export const ProductsContainerWrapper = styled.View`
  flex-direction: row;
  width: 100%;
`;

export const ProductsContent = styled.View`
  flex: 11;
  justify-content: center;
  align-items: flex-start;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const CartContent = styled.View`
  flex: 5;
  justify-content: center;
  align-items: flex-start;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const HR = styled.View`
  position: absolute;
  bottom: -20px;
  width: 100%;
  border-bottom-width: 4px;
  border-bottom-color: ${({ theme }) => theme.colors.bottomTabBar.background};
  border-radius: 4px;
  margin-top: ${RFValue(18)}px;
  margin-bottom: ${RFValue(12)}px;
`;

export const ProductCard = styled.TouchableOpacity`
  width: 30%;
  background-color: ${({ theme }) => theme.colors.backgroundGrayLight};
  align-items: center;
  justify-content: center;
  border-radius: 22px;
  margin: 5px 5px 30px;
`;

export const ProductImage = styled.Image.attrs(() => ({
  height: 100,
  width: 100,
}))`
  top: -30px;
  border-radius: 16px;
  height: 100px;
  width: 100px;
`;

export const SemImagemProductImg = styled(SemImagemImage).attrs(() => ({
  height: 100,
}))`
  top: -30px;
  width: 100px;
`;

export const ProductTitle = styled.Text`
  top: -30px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.black};
  font-size: ${RFValue(13)}px;
  line-height: ${RFValue(18)}px;
  text-align: center;
  height: ${RFValue(36)}px;
`;

export const ProductPrices = styled.View`
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const ProductPriceTo = styled.Text`
  color: ${({ theme }) => theme.colors.primary};
  font-size: ${RFValue(18)}px;
  line-height: ${RFValue(22)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  top: -20px;
  text-align: center;
`;

export const ProductPriceFrom = styled.Text`
  color: ${({ theme }) => theme.colors.textLight};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(18)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
  top: -20px;
  text-align: center;
  text-decoration: line-through;
`;

export const SearchResultWrapper = styled.View`
  flex-direction: column;
  margin-top: 60px;
  justify-content: center;
  align-items: center;
  width: 80%;
`;

export const SearchResultImages = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const SearchResultText = styled.Text`
  text-align: center;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.primary};
  font-size: ${RFValue(20)}px;
  line-height: ${RFValue(26)}px;
  padding-top: 20px;
  flex-wrap: wrap;
`;
