import { useNavigation, useRoute } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { ICategory, IProduct } from '../../@types/interfaces';
import theme from '../../global/styles/theme';
import api from '../../services/api';
import {
  getAsyncStorage,
  multiRemoveAsyncStorage,
} from '../../utils/asyncStorage';
import { EnumFidelidade, EnumTipoPedido } from '../../utils/constants';
import { formatPrice } from '../../utils/price';
import * as S from './styles';

type ParamsProps = {
  id: number;
};

export const CategoriaLista = () => {
  const [loading, setLoading] = useState(false);
  const [branchId, setBranchId] = useState('');
  const [categoryId, setCategoryId] = useState(0);
  const [selectedCategory, setSelectedCategory] = useState<ICategory>(
    {} as ICategory
  );
  const [categories, setCategories] = useState<ICategory[]>([]);
  const [products, setProducts] = useState<IProduct[]>([]);

  const navigation = useNavigation();
  const route = useRoute();

  async function handleBackClick() {
    await multiRemoveAsyncStorage();
    navigation.navigate('Categorias');
  }

  async function handleSearchClick() {
    navigation.navigate('Busca');
  }

  // Verifica se o usuario possui as variaveis necessarias ate essa pagina
  async function checkUserSelections() {
    const loja = await getAsyncStorage('loja');
    if (!loja) {
      navigation.navigate('Loja');
    }
    setBranchId(loja);

    const tipoPedido: EnumTipoPedido = await getAsyncStorage('tipoPedido');
    if (!tipoPedido) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Local');
    }

    const fidelidade: EnumFidelidade = await getAsyncStorage('fidelidade');
    if (!fidelidade) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Fidelidade');
    }
  }

  useEffect(() => {
    checkUserSelections();
  }, []);

  async function getCategories() {
    setLoading(true);
    try {
      const { id: categoryId } = route.params as ParamsProps;
      setCategoryId(categoryId);

      const response = await api.get(`/categories`);
      const categoriesList = response.data;
      const checkIfSelectedCategoryExists = categoriesList.filter(
        (category: ICategory) => category.number === categoryId
      );

      if (checkIfSelectedCategoryExists.length !== 1) {
        navigation.navigate('Categorias');
        return;
      }

      setSelectedCategory(checkIfSelectedCategoryExists[0]);
      setCategories(categoriesList);
    } catch (error) {
      console.error('Error Categories()', error);
    }

    console.log('-------- INI --------');
    const loja = await getAsyncStorage('loja');
    console.log('loja', loja);
    const tipoPedido = await getAsyncStorage('tipoPedido');
    console.log('tipoPedido', tipoPedido);
    const fidelidade = await getAsyncStorage('fidelidade');
    console.log('fidelidade', fidelidade);
    const cpf = await getAsyncStorage('cpf');
    console.log('cpf', cpf);
    console.log('-------- FIM --------');

    setLoading(false);
  }

  useEffect(() => {
    getCategories();
  }, []);

  function handleChangeCategory(id: number) {
    setLoading(true);
    setCategoryId(id);

    const checkIfSelectedCategoryExists = categories.filter(
      (category: ICategory) => category.number === id
    );

    if (checkIfSelectedCategoryExists.length !== 1) {
      navigation.navigate('Categorias');
      setLoading(false);
      return;
    }

    setSelectedCategory(checkIfSelectedCategoryExists[0]);
    setLoading(false);
  }

  async function getProducts() {
    try {
      console.log(
        `/products?category_id=${selectedCategory.number}&is_active=true&branch_id=${branchId}`
      );
      const response = await api.get(
        `/products?category_id=${categoryId}&is_active=true&branch_id=${branchId}`
      );
      setProducts(response.data);
      console.log('>>', response.data);
    } catch (error) {}
  }

  useEffect(() => {
    getProducts();
  }, [selectedCategory]);

  function handleOpenProductDetails(id: string) {
    navigation.navigate('Detalhes', { id });
  }

  return (
    <S.Container colors={['#FA2458', '#FC4A4E', '#FED278']} start={[0.8, 0.3]}>
      <S.LogoImg />
      <S.Header>
        <S.HeaderIcon onPress={() => handleBackClick()}>
          <S.BackImg />
        </S.HeaderIcon>

        <S.CategoryHorizontalList
          horizontal
          showsHorizontalScrollIndicator={false}
        >
          {categories.length > 0 &&
            categories.map((category, i) => (
              <S.CategoryCard
                key={`${category.number}-${i}`}
                onPress={() => handleChangeCategory(category.number)}
              >
                {category.thumbnail_url ? (
                  <S.CategoryImage source={{ uri: category.thumbnail_url }} />
                ) : (
                  <S.SemImagemImg />
                )}
                <S.CategoryTitle numberOfLines={2}>
                  {category.name}
                </S.CategoryTitle>

                {category.number === categoryId && <S.HR />}
              </S.CategoryCard>
            ))}
        </S.CategoryHorizontalList>

        <S.HeaderIcon onPress={() => handleSearchClick()}>
          <S.SearchImg />
        </S.HeaderIcon>
      </S.Header>

      {loading ? (
        <ActivityIndicator
          size='large'
          color={theme.colors.submit.primary.color}
          style={{ marginTop: 50 }}
        />
      ) : (
        <S.ProductsContainer>
          <S.ProductsContainerWrapper>
            <S.ProductsContent>
              <S.InfoTitle>{selectedCategory.name}</S.InfoTitle>
              {products.length > 0 ? (
                products.map((product, i) => {
                  let priceFrom = null;
                  let priceTo = null;

                  if (product.promotions[0]?.value) {
                    priceTo = formatPrice(product.promotions[0]?.value);
                    priceFrom = formatPrice(product.product_prices[0].price);
                  } else {
                    priceTo = formatPrice(product.product_prices[0].price);
                  }

                  // if (i > 2 && i < 13) {
                  //   priceTo = formatPrice(120.9);
                  //   priceFrom = formatPrice(100.9);
                  // }

                  return (
                    <S.ProductCard
                      key={`${product.id}-${i}`}
                      onPress={() => handleOpenProductDetails(product.id)}
                    >
                      {product.thumbnail_url ? (
                        <S.ProductImage
                          source={{ uri: product.thumbnail_url }}
                        />
                      ) : (
                        <S.SemImagemProductImg />
                      )}
                      <S.ProductTitle numberOfLines={2}>
                        {product.name}
                      </S.ProductTitle>

                      <S.ProductPrices>
                        <S.ProductPriceTo>R$ {priceTo}</S.ProductPriceTo>
                        {priceFrom && (
                          <S.ProductPriceFrom>
                            R$ {priceFrom}
                          </S.ProductPriceFrom>
                        )}
                      </S.ProductPrices>
                    </S.ProductCard>
                  );
                })
              ) : (
                <S.SearchResultWrapper>
                  <S.SearchResultImages>
                    <S.Result1Img />
                    <S.Result2Img />
                    <S.Result3Img />
                    <S.Result4Img />
                  </S.SearchResultImages>
                  <S.SearchResultText>Novidades em breve!</S.SearchResultText>
                </S.SearchResultWrapper>
              )}
            </S.ProductsContent>

            <S.CartContent>
              <S.InfoTitle>Seu Carrinho</S.InfoTitle>
            </S.CartContent>
          </S.ProductsContainerWrapper>
        </S.ProductsContainer>
      )}
    </S.Container>
  );
};
