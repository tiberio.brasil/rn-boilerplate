import styled from "styled-components/native";

export const Container = styled.View`
  flex: 1;
  height: 100%;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const Content = styled.View`
  flex: 1;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.background};
  padding: 28px 16px 0;
  border-top-left-radius: 24px;
  border-top-right-radius: 24px;
`;

export const Logo = styled.Image`
  margin: 30px;
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
`;

export const BackButtonArea = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  border-radius: 99px;
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px;
`;

export const TitlePedido = styled.Text`
  font-size: 32px;
  font-family: ${({ theme }) => theme.fonts.regular};
  text-align: center;
  width: 90%;
`;

export const ArrowLeft = styled.Image``;
export const ImageProduct = styled.Image`
  width: 43px;
  height: 43px;
`;

export const OptionsArea = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
`;

export const ButtonConfirm = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.primary};
  border-radius: 49px;
  width: 350px;
  height: 72px;
  align-items: center;
  justify-content: center;
  margin-top: 40px;
`;

export const ButtonConfirmText = styled.Text`
  color: ${({ theme }) => theme.colors.white};
  font-size: 22px;
  font-family: ${({ theme }) => theme.fonts.regular};
  margin-left: 16px;
`;

export const TitleButton = styled.Text`
  font-size: 22px;
  font-family: ${({ theme }) => theme.fonts.regular};
`;

export const CancelButton = styled.TouchableOpacity`
  border-radius: 50px;
  align-items: center;
  justify-content: center;
  height: 72px;
  width: 350px;
  background-color: ${({ theme }) => theme.colors.textLight};
  margin-top: 30px;
`;

export const Subtitle = styled.Text`
  font-size: 24px;
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.input.text};
`;