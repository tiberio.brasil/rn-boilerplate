import React, { useEffect } from 'react';
import * as S from './styles';

import { useNavigation } from '@react-navigation/native';
import ArrowLeft from '../../assets/icons/arrow-left-white.png';
import Logo from '../../assets/image/logo.png';
import {
  getAsyncStorage,
  multiRemoveAsyncStorage,
} from '../../utils/asyncStorage';
import { EnumTipoPedido } from '../../utils/constants';

export const Identificacao = () => {
  const navigation = useNavigation();

  // Verifica se o usuario possui as variaveis necessarias ate essa pagina
  async function checkUserSelections() {
    const loja = await getAsyncStorage('loja');
    if (!loja) {
      navigation.navigate('Loja');
    }

    const tipoPedido: EnumTipoPedido = await getAsyncStorage('tipoPedido');
    if (!tipoPedido) {
      await multiRemoveAsyncStorage();
      navigation.navigate('Local');
    }
  }

  useEffect(() => {
    checkUserSelections();
  }, []);

  return (
    <S.Container>
      <S.Logo source={Logo} />

      <S.Content>
        <S.Header>
          <S.BackButtonArea>
            <S.ArrowLeft source={ArrowLeft} />
          </S.BackButtonArea>
          <S.TitlePedido>CPF na nota</S.TitlePedido>
        </S.Header>

        <S.OptionsArea>
          <S.Subtitle>Deseja seu CPF na nota ?</S.Subtitle>
          <S.ButtonConfirm>
            <S.ButtonConfirmText>Sim</S.ButtonConfirmText>
          </S.ButtonConfirm>
          <S.CancelButton>
            <S.TitleButton>Não, obrigado</S.TitleButton>
          </S.CancelButton>
        </S.OptionsArea>
      </S.Content>
    </S.Container>
  );
};
