import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';
import { LinearGradient } from 'expo-linear-gradient';
import LogoImage from '../../assets/svg/logo.svg';

export const LogoImg = styled(LogoImage).attrs(() => ({
  width: '26%',
  height: 180,
}))``;

export const Container = styled(LinearGradient)`
  flex: 1;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`;

export const ButtonsWrapper = styled.ScrollView`
  flex-direction: column;
  width: 100%;
`;

export const ButtonsOptions = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.white};
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 5px 10px;
  border-radius: 49px;
  margin: 10px;
`;

export const ButtonOptionsText = styled.Text`
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(19)}px;
  color: ${({ theme }) => theme.colors.primary};
  font-family: ${({ theme }) => theme.fonts.alternative.bold};
`;

export const QuestionText = styled.Text`
  font-size: ${RFValue(20)}px;
  line-height: ${RFValue(29)}px;
  color: ${({ theme }) => theme.colors.background};
  font-family: ${({ theme }) => theme.fonts.regular};
`;
