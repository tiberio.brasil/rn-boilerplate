import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { IBranch } from '../../@types/interfaces';
import theme from '../../global/styles/theme';
import api from '../../services/api';
import { setAsyncStorage } from '../../utils/asyncStorage';
import * as S from './styles';

export const Loja = () => {
  const [loading, setLoading] = useState(false);
  const [branches, setBranches] = useState<IBranch[]>([] as IBranch[]);

  const navigation = useNavigation();

  async function handleSelectOption(value: string) {
    await setAsyncStorage('loja', value);
    navigation.navigate('SplashScreen');
  }

  async function getBranches() {
    setLoading(true);
    try {
      const response = await api.get(`/branches`);
      setBranches(response.data);
    } catch (error) {
      console.error('Error getBranches()', error);
    }
    setLoading(false);
  }

  useEffect(() => {
    getBranches();
  }, []);

  return (
    <S.Container colors={['#FA2458', '#FC4A4E', '#FED278']} start={[0.8, 0.3]}>
      <S.LogoImg />

      <S.QuestionText>
        Área administrativa - Selecione a sua loja:
      </S.QuestionText>

      <S.ButtonsWrapper>
        {loading ? (
          <ActivityIndicator
            size='large'
            color={theme.colors.submit.primary.color}
            style={{ marginTop: 50 }}
          />
        ) : (
          branches.length > 0 &&
          branches.map((branch) => {
            return (
              <S.ButtonsOptions
                onPress={() => handleSelectOption(branch.id)}
                key={branch.id}
              >
                <S.ButtonOptionsText>
                  {branch.street}, {branch.number} - {branch.district} -{' '}
                  {branch.city}
                </S.ButtonOptionsText>
              </S.ButtonsOptions>
            );
          })
        )}
      </S.ButtonsWrapper>
    </S.Container>
  );
};
