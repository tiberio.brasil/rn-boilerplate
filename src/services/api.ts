import axios from "axios";

const api = axios.create({
  baseURL: "https://sucre-dev.teclat.dev/api",
});

export default api;
